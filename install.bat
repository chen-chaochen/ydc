@echo off 
:lan
cls
echo Language
echo 1 = English ,  2  =  Chinese 
set/p "lan=Yuron Install Guide>"
if %lan% == 1 goto En
if %lan% == 2 goto Cn
goto Lan
:En
echo Yuron Dev-collections install guide!
echo We will install all kits to your computer in english
echo If yes ,pause ENTER to start installing
set/p "a="
echo Start conf files............
copy .\conf\en\*.conf .\conf\*.conf
echo Confing is OK
echo Start Compile Support Lib
echo Input the libfile-name in the [source\en]
echo Input OK to end this step.
echo eg: Console
:eloop
set/p "file=>"
if %file% == OK goto Eloop2
.\yuronc source\en\%file%
copy .\source\en\%file%.yl_app .\libroot\yuron\%file%
del .\source\en\%file%.yl_app
goto eloop
:Eloop2
echo Yuron Dev-collections have been Installed!
set/p "a="
exit
:Cn
echo 玉龙开发组件安装向导
echo 我们将向您的计算机中安装所需组件
echo 按任意键以开始安装
set/p "a="
echo 开始配置文件
copy .\conf\cn\*.conf .\conf\*.conf
echo 已经完成配置
echo 开始编译基本类
echo 请输入要编译的类 (应存在于.\source\cn\中)
echo 输入OK以结束此步
echo 示例: 控制台
:cloop
set/p "file=>"
if %file% == OK goto cloop2
.\yuronc source\cn\%file%
copy .\source\cn\%file%.yl_app .\libroot\玉龙\%file%
del .\source\cn\%file%.yl_app
goto cloop
:cloop2
echo 已安装玉龙开发组件!
set/p "a="
exit
