push 将常量压栈
pop 弹栈
add a 将a压栈
mov a 将栈顶元素存入a中(不弹栈)
loop 循环指令
logic 逻辑指令
thread 线程指令
end 结束指令
exit 跳出指令
api API指令




api:
+
-
*
/
=
&
\ 余数
| 取整
cli.
 print 输出
 read  读取
 readline  读取一行
 getkey 等待按键
 readkey 读取键盘缓冲区
 locate(x,y) 定位光标
 cls
 color(前景,背景色) 详见文件 support/控制台调色板
 viewspace(x1,x2) 设定输出区域 X1 TO X2
 csrlin 光标行
 pos  光标列
 pause 暂停直到按键

array.
 newarry(name) 创建名为 name 的数组
 getarry(name,locate) 从name数组的locate位置获取值
 setarry(name,loc,val) 将name数组的loc位置设置为val值
 //有关数组,详见 support/数组

date.
 date 日期
 time 时间
 setdate(a) 设定日期a
 settime(a) 设定时间a
 timer 返回计时器计时值
file.
 free 返回最小空闲编号(一般以2开始,解释器会将源文件设定为1)
 open(file,model[input/output/append/random/binary],filenumber) 
 close(num)
 input(num) 从num号文件读
 lineinput（num）  从num号文件读一行
 print(num,str)  向num号文件写入str
 put(num,pos,str) 向num号文件(random/binary)从pos位置写入str
 get(num,pos) 从num号文件(random/binary)pos位置读取
 getseek(num) 获取num号文件下一位置
 setseek(num,pos) 将num号文件定位到pos位置
 lof(num) num号文件大小
 eof(num) 是否到达num号文件尾(true = -1)
 loc(num) num号文件上一位置
 kill/rm(file) 删除file文件
 rename(a,b) 将a文件重命名为b
 mkdir(name) 创建name路径
 rmdir(name) 删除name路径
math.
 abs(n) n的绝对值
 exp(n) e的n次幂
 log(n) n的ln值
 fix(n) n的整数部分
 frac(n) n的小数部分
 int(n) 小于或等于n的最大整数
 sgn(n) n的符号
 sin/asin(n) n的sin/asin值
 cos/acos(n) n的cos/acos值
 tan/atan(n) n的tan/cot值
 bin(n) n的2进制
 hex(n) n的16进制
ran.
 random 以时间为种子设置随机数
 new [0,1)之间的随机值 
 newint [0,10)之间的随机整数
sys.
 exec(f,c) 将控制权暂时交给以c为参数的f应用
 sleep(time) 等待time(毫秒)时间
 curdir 工作目录
 chdir(a) 设定工作目录为a
 exename 获得程序名
 exepath 获得YL_app的工作目录/本地化后为程序位置
 command() 获取命令行参数(对于yl_app,需要手动输入)
 rm(file) 删除file文件
 rename(a,b) 将a文件重命名为b
 exsist(a) 检测a是否存在,存在返回-1 不存在返回0
 copyfile(a,b) 复制a 到b
 datetime(a) 获取a的最后修改时间
 mkdir(name) 创建name路径
 rmdir(name) 删除name路径
 freeram RAM空间
 evar(a) 获得环境变量a的值
 setvar(a) 设定环境变量a
 shell(a) 向系统发送指令a
 return（a） 结束程序并返回值a
string.
 len(s) 字符串s的长度
 asc(s) 字符S的ASCII值
 chr(s) S转换为字符 
 mid(a,b,c) 从a中b位置读取c个字符
 lcase(s) 小写s
 ucase(s) 大写s
 instr(start,s,sign) 从s左第start个字符开始,第一次出现sign的位置
 rinstr(start,s,sign) 从s右第start个字符开始,第一次出现sign的位置 
 replace(a,b,c) 将 a 中所有 b 替换为c


 