### yuron_exe 

#### 设计目标
<br>
    将Yuron APP跨平台生成可以直接运行的本地可执行程序。


#### 用法
<br>
    1.安装YDC 0.3.6及以上版本(JadeDragon_SP3以上版本)
<br>
    2. 在编译源文件获得可以执行的APP后, 命令行 yuron_exe_[平台]

#### 可选参数

win32

win64

linux-x86    开发中

linux-x86_64 开发中

linux-raspi    开发中

PS:yuron_exe 支持生成非本机系统的可执行程序。win32用户也可生成win64，linux程序