/*
yuron is licensed under Yulong PSL v1.
https://gitee.com/chen-chaochen/yulong-open-source-license/releases/1.0
*/
typedef   signed char       int8;
typedef unsigned char      uint8;
typedef   signed short      int16;
typedef unsigned short     uint16;
typedef   signed int        int32;
typedef unsigned int       uint32;
typedef   signed long long  int64;
typedef unsigned long long uint64;
typedef struct { char *data; int64 len; int64 size; } ylSTRING;
typedef int8 boolean;
#define yl_D2I( value ) ((int32)__builtin_nearbyint( value ))
#define yl_D2L( value ) ((int64)__builtin_nearbyint( value ))
int32 yl_ConsoleView( int32, int32 );
int32 yl_Locate( int32, int32, int32, int32, int32 );
int32 yl_GetX( void );
int32 yl_GetY( void );
void yl_Cls( int32 );
int32 yl_Color( int32, int32, int32 );
ylSTRING* yl_Inkey( void );
int32 yl_Getkey( void );
int32 yl_ErrorGetNum( void );
int32 yl_FileOpen( ylSTRING*, uint32, uint32, uint32, int32, int32 );
int32 yl_FileOpenEncod( ylSTRING*, uint32, uint32, uint32, int32, int32, uint8* );
int32 yl_FileClose( int32 );
int32 yl_FilePutStrLarge( int32, int64, void*, int64 );
int32 yl_FileGetStrLarge( int32, int64, void*, int64 );
int64 yl_FileTell( int32 );
int32 yl_FileSeekLarge( int32, int64 );
int32 yl_FileLineInput( int32, void*, int64, int32 );
int32 yl_LineInput( ylSTRING*, void*, int64, int32, int32, int32 );
int32 yl_FileInput( int32 );
int32 yl_ConsoleInput( ylSTRING*, int32, int32 );
int32 yl_InputString( void*, int64, int32 );
int32 rename( uint8*, uint8* );
int32 yl_FileFree( void );
int32 yl_FileEof( int32 );
int32 yl_FileKill( ylSTRING* );
int64 yl_FileSize( int32 );
int64 yl_FileLocation( int32 );
void yl_Randomize( double, int32 );
double yl_Rnd( float );
int32 yl_SGNDouble( double );
double yl_FIXDouble( double );
double yl_FRACd( double );
uint64 yl_GetMemAvail( int32 );
void yl_PrintString( int32, ylSTRING*, int32 );
ylSTRING* yl_StrInit( void*, int64, void*, int64, int32 );
ylSTRING* yl_StrAssign( void*, int64, void*, int64, int32 );
void yl_StrDelete( ylSTRING* );
ylSTRING* yl_StrConcat( ylSTRING*, void*, int64, void*, int64 );
int32 yl_StrCompare( void*, int64, void*, int64 );
ylSTRING* yl_StrConcatAssign( void*, int64, void*, int64, int32 );
ylSTRING* yl_StrAllocTempResult( ylSTRING* );
ylSTRING* yl_StrAllocTempDescZEx( uint8*, int64 );
ylSTRING* yl_IntToStr( int32 );
ylSTRING* yl_UIntToStr( uint32 );
ylSTRING* yl_LongintToStr( int64 );
ylSTRING* yl_ULongintToStr( uint64 );
ylSTRING* yl_DoubleToStr( double );
ylSTRING* yl_StrMid( ylSTRING*, int64, int64 );
int64 yl_StrLen( void*, int64 );
uint32 yl_ASC( ylSTRING*, int64 );
ylSTRING* yl_CHR( int32, ... );
int64 yl_StrInstr( int64, ylSTRING*, ylSTRING* );
int64 yl_StrInstrRev( ylSTRING*, ylSTRING*, int64 );
double yl_VAL( ylSTRING* );
int32 yl_VALINT( ylSTRING* );
int64 yl_VALLNG( ylSTRING* );
ylSTRING* yl_HEX_l( uint64 );
ylSTRING* yl_BIN_l( uint64 );
ylSTRING* yl_StrLcase2( ylSTRING*, int32 );
ylSTRING* yl_StrUcase2( ylSTRING*, int32 );
void yl_End( int32 );
ylSTRING* yl_Command( int32 );
ylSTRING* yl_CurDir( void );
ylSTRING* yl_ExePath( void );
double yl_Timer( void );
ylSTRING* yl_Time( void );
ylSTRING* yl_Date( void );
int32 yl_Shell( ylSTRING* );
void yl_End( int32 );
int32 yl_Exec( ylSTRING*, ylSTRING* );
ylSTRING* yl_GetEnviron( ylSTRING* );
int32 yl_SetEnviron( ylSTRING* );
int32 yl_SleepEx( int32, int32 );
int32 yl_SetTime( ylSTRING* );
int32 yl_SetDate( ylSTRING* );
int32 yl_MkDir( ylSTRING* );
int32 yl_RmDir( ylSTRING* );
int32 yl_ChDir( ylSTRING* );
static void yl_ctor__yuron( void ) __attribute__(( constructor ));
void PUSH( ylSTRING* );
void POP( void );
ylSTRING* TOP( void );
void MOV( ylSTRING*, ylSTRING* );
void ADD( ylSTRING* );
void HOSTFILE( ylSTRING*, int32, ylSTRING*, ylSTRING*, int64* );
ylSTRING* API( ylSTRING* );
ylSTRING* REPLACE( ylSTRING*, ylSTRING*, ylSTRING* );
ylSTRING* TRMATE( ylSTRING* );
static ylSTRING STACK[17];
static ylSTRING VANAME[10000001];
static ylSTRING VAVALUE[10000001];
static int32 CONF;
static ylSTRING MAINENDSIGN;
static ylSTRING MAINFILE;

void VMD( void )
{
	label2:;
	double I1;
	__builtin_memset( &I1, 0, 8ll );
	{
		I1 = 0x1.p+0;
		label7:;
		{
			yl_StrAssign( (void*)((int64)(ylSTRING*)VANAME + (yl_D2L( I1 ) * 24ll)), -1ll, (void*)"", 1ll, 0 );
		}
		label5:;
		I1 = I1 + 0x1.p+0;
		label4:;
		if( I1 <= 0x1.312Dp+23 ) goto label7;
		label6:;
	}
	label3:;
}

ylSTRING* REPLACE( ylSTRING* A1, ylSTRING* B1, ylSTRING* C1 )
{
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label8:;
	double I1;
	__builtin_memset( &I1, 0, 8ll );
	ylSTRING REP1;
	__builtin_memset( &REP1, 0, 24ll );
	I1 = 0x1.p+0;
	yl_StrAssign( (void*)&REP1, -1ll, (void*)"", 1ll, 0 );
	label10:;
	{
		int64 vr4 = yl_StrLen( (void*)B1, -1ll );
		ylSTRING* vr6 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), vr4 );
		int32 vr7 = yl_StrCompare( (void*)vr6, -1ll, (void*)B1, -1ll );
		if( (int64)vr7 != 0ll ) goto label14;
		{
			ylSTRING TMP53;
			int64 vr9 = yl_StrLen( (void*)B1, -1ll );
			I1 = (I1 + (double)vr9) + -0x1.p+0;
			__builtin_memset( &TMP53, 0, 24ll );
			ylSTRING* vr16 = yl_StrConcat( &TMP53, (void*)&REP1, -1ll, (void*)C1, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr16, -1ll, 0 );
		}
		goto label13;
		label14:;
		{
			ylSTRING TMP63;
			ylSTRING* vr19 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), 1ll );
			__builtin_memset( &TMP63, 0, 24ll );
			ylSTRING* vr23 = yl_StrConcat( &TMP63, (void*)&REP1, -1ll, (void*)vr19, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr23, -1ll, 0 );
		}
		label13:;
		I1 = I1 + 0x1.p+0;
		int64 vr26 = yl_StrLen( (void*)A1, -1ll );
		if( I1 < (double)vr26 ) goto label16;
		{
			ylSTRING TMP73;
			ylSTRING* vr29 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), 1ll );
			__builtin_memset( &TMP73, 0, 24ll );
			ylSTRING* vr33 = yl_StrConcat( &TMP73, (void*)&REP1, -1ll, (void*)vr29, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr33, -1ll, 0 );
			goto label11;
		}
		label16:;
		label15:;
	}
	label12:;
	goto label10;
	label11:;
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)&REP1, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&REP1 );
	goto label9;
	yl_StrDelete( (ylSTRING*)&REP1 );
	label9:;
	ylSTRING* vr40 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr40;
}

void PUSH( ylSTRING* A1 )
{
	label17:;
	int32 I1;
	__builtin_memset( &I1, 0, 4ll );
	{
		I1 = 15;
		label22:;
		{
			yl_StrAssign( (void*)(((int64)(ylSTRING*)STACK + ((int64)I1 * 24ll)) + 24ll), -1ll, (void*)((int64)(ylSTRING*)STACK + ((int64)I1 * 24ll)), -1ll, 0 );
		}
		label20:;
		I1 = (int32)((int64)I1 + -1ll);
		label19:;
		if( (int64)I1 >= 1ll ) goto label22;
		label21:;
	}
	yl_StrAssign( (void*)((uint8*)STACK + 24ll), -1ll, (void*)A1, -1ll, 0 );
	label18:;
}

void POP( void )
{
	label23:;
	int32 I1;
	__builtin_memset( &I1, 0, 4ll );
	{
		I1 = 2;
		label28:;
		{
			yl_StrAssign( (void*)(((int64)(ylSTRING*)STACK + ((int64)I1 * 24ll)) + -24ll), -1ll, (void*)((int64)(ylSTRING*)STACK + ((int64)I1 * 24ll)), -1ll, 0 );
		}
		label26:;
		I1 = (int32)((int64)I1 + 1ll);
		label25:;
		if( (int64)I1 <= 16ll ) goto label28;
		label27:;
	}
	yl_StrAssign( (void*)((uint8*)STACK + 384ll), -1ll, (void*)"", 1ll, 0 );
	label24:;
}

ylSTRING* TOP( void )
{
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label29:;
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)((uint8*)STACK + 24ll), -1ll, 0 );
	goto label30;
	label30:;
	ylSTRING* vr3 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr3;
}

void MOV( ylSTRING* VNAME1, ylSTRING* VALUE1 )
{
	label31:;
	int64 I1;
	__builtin_memset( &I1, 0, 8ll );
	{
		I1 = 1ll;
		label36:;
		{
			int32 vr3 = yl_StrCompare( (void*)((int64)(ylSTRING*)VANAME + (I1 * 24ll)), -1ll, (void*)VNAME1, -1ll );
			int32 vr8 = yl_StrCompare( (void*)((int64)(ylSTRING*)VANAME + (I1 * 24ll)), -1ll, (void*)"", 1ll );
			if( ((int64)-((int64)vr3 == 0ll) | (int64)-((int64)vr8 == 0ll)) == 0ll ) goto label38;
			{
				yl_StrAssign( (void*)((int64)(ylSTRING*)VANAME + (I1 * 24ll)), -1ll, (void*)VNAME1, -1ll, 0 );
				yl_StrAssign( (void*)((int64)(ylSTRING*)VAVALUE + (I1 * 24ll)), -1ll, (void*)VALUE1, -1ll, 0 );
				goto label35;
			}
			label38:;
			label37:;
		}
		label34:;
		I1 = I1 + 1ll;
		label33:;
		if( I1 <= 10000000ll ) goto label36;
		label35:;
	}
	label32:;
}

void ADD( ylSTRING* VNAME1 )
{
	label39:;
	double I1;
	__builtin_memset( &I1, 0, 8ll );
	{
		I1 = 0x1.p+0;
		label44:;
		{
			int32 vr4 = yl_StrCompare( (void*)((int64)(ylSTRING*)VANAME + (yl_D2L( I1 ) * 24ll)), -1ll, (void*)VNAME1, -1ll );
			int32 vr10 = yl_StrCompare( (void*)((int64)(ylSTRING*)VANAME + (yl_D2L( I1 ) * 24ll)), -1ll, (void*)"", 1ll );
			if( ((int64)-((int64)vr4 == 0ll) | (int64)-((int64)vr10 == 0ll)) == 0ll ) goto label46;
			{
				PUSH( (ylSTRING*)((int64)(ylSTRING*)VAVALUE + (yl_D2L( I1 ) * 24ll)) );
				goto label43;
			}
			label46:;
			label45:;
		}
		label42:;
		I1 = I1 + 0x1.p+0;
		label41:;
		if( I1 <= 0x1.312Dp+23 ) goto label44;
		label43:;
	}
	label40:;
}

void HOSTFILE( ylSTRING* FILE1, int32 FILENUMBER1, ylSTRING* HEAD1, ylSTRING* ESGIN1, int64* BEGINNUMBER1 )
{
	label47:;
	ylSTRING SIGN1;
	__builtin_memset( &SIGN1, 0, 24ll );
	ylSTRING LABLE1;
	__builtin_memset( &LABLE1, 0, 24ll );
	ylSTRING SIGN21;
	__builtin_memset( &SIGN21, 0, 24ll );
	int64 F1;
	__builtin_memset( &F1, 0, 8ll );
	int64 F21;
	__builtin_memset( &F21, 0, 8ll );
	int32 I1;
	__builtin_memset( &I1, 0, 4ll );
	F1 = (int64)FILENUMBER1;
	yl_FileClose( (int32)F1 );
	yl_FileOpenEncod( (ylSTRING*)FILE1, 2u, 0u, 0u, (int32)F1, 0, (uint8*)"ascii" );
	yl_FileSeekLarge( (int32)F1, *BEGINNUMBER1 );
	label49:;
	{
		ylSTRING TMP112;
		ylSTRING TMP122;
		yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
		__builtin_memset( &TMP112, 0, 24ll );
		ylSTRING* vr15 = yl_StrConcat( &TMP112, (void*)"<", 2ll, (void*)HEAD1, -1ll );
		__builtin_memset( &TMP122, 0, 24ll );
		ylSTRING* vr18 = yl_StrConcat( &TMP122, (void*)vr15, -1ll, (void*)">", 2ll );
		int32 vr20 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)vr18, -1ll );
		if( (int64)vr20 != 0ll ) goto label53;
		{
			goto label50;
		}
		label53:;
		label52:;
	}
	label51:;
	goto label49;
	label50:;
	label54:;
	if( (int64)CONF != 0ll ) goto label55;
	{
		ylSTRING TMP162;
		ylSTRING TMP172;
		int32 vr23 = yl_ErrorGetNum(  );
		int32 vr27 = yl_FileEof( (int32)F1 );
		if( ((int64)-((int64)vr23 > 0ll) | (int64)-((int64)vr27 != 0ll)) == 0ll ) goto label57;
		{
			ylSTRING TMP143;
			int32 vr31 = yl_ErrorGetNum(  );
			ylSTRING* vr35 = yl_LongintToStr( ((int64)vr31 * 3ll) + 1ll );
			__builtin_memset( &TMP143, 0, 24ll );
			ylSTRING* vr38 = yl_StrConcat( &TMP143, (void*)"Err Unexpected:", 16ll, (void*)vr35, -1ll );
			yl_PrintString( 0, (ylSTRING*)vr38, 1 );
			yl_End( 0 );
		}
		label57:;
		label56:;
		yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
		int32 vr42 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"exit", 5ll );
		if( (int64)vr42 != 0ll ) goto label59;
		{
			CONF = 1;
		}
		label59:;
		label58:;
		__builtin_memset( &TMP162, 0, 24ll );
		ylSTRING* vr46 = yl_StrConcat( &TMP162, (void*)"<", 2ll, (void*)ESGIN1, -1ll );
		__builtin_memset( &TMP172, 0, 24ll );
		ylSTRING* vr49 = yl_StrConcat( &TMP172, (void*)vr46, -1ll, (void*)">", 2ll );
		int32 vr51 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)vr49, -1ll );
		if( (int64)vr51 != 0ll ) goto label61;
		{
			yl_StrDelete( (ylSTRING*)&SIGN21 );
			yl_StrDelete( (ylSTRING*)&LABLE1 );
			yl_StrDelete( (ylSTRING*)&SIGN1 );
			goto label48;
		}
		label61:;
		label60:;
		int32 vr57 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"goto", 5ll );
		if( (int64)vr57 != 0ll ) goto label63;
		{
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			yl_StrAssign( (void*)&LABLE1, -1ll, (void*)&SIGN1, -1ll, 0 );
			yl_FileClose( (int32)F1 );
			yl_FileOpenEncod( (ylSTRING*)FILE1, 2u, 0u, 0u, (int32)F1, 0, (uint8*)"ascii" );
			label64:;
			int32 vr66 = yl_FileEof( (int32)F1 );
			if( (int64)vr66 != 0ll ) goto label65;
			{
				ylSTRING TMP204;
				yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
				__builtin_memset( &TMP204, 0, 24ll );
				ylSTRING* vr73 = yl_StrConcat( &TMP204, (void*)"lable:", 8ll, (void*)&LABLE1, -1ll );
				int32 vr75 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)vr73, -1ll );
				if( (int64)vr75 != 0ll ) goto label67;
				{
					goto label65;
					label67:;
				}
				int32 vr77 = yl_ErrorGetNum(  );
				if( (int64)vr77 <= 0ll ) goto label69;
				{
					ylSTRING TMP215;
					int32 vr79 = yl_ErrorGetNum(  );
					ylSTRING* vr83 = yl_LongintToStr( ((int64)vr79 * 3ll) + 1ll );
					__builtin_memset( &TMP215, 0, 24ll );
					ylSTRING* vr86 = yl_StrConcat( &TMP215, (void*)"Err Unexpected:", 16ll, (void*)vr83, -1ll );
					yl_PrintString( 0, (ylSTRING*)vr86, 1 );
					yl_End( 0 );
				}
				label69:;
				label68:;
			}
			goto label64;
			label65:;
		}
		label63:;
		label62:;
		int32 vr88 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"api", 4ll );
		if( (int64)vr88 != 0ll ) goto label71;
		{
			ylSTRING TMP233;
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			__builtin_memset( &TMP233, 0, 24ll );
			ylSTRING* vr94 = API( &SIGN1 );
			yl_StrAssign( (void*)&TMP233, -1ll, (void*)vr94, -1ll, 0 );
			PUSH( &TMP233 );
			yl_StrDelete( (ylSTRING*)&TMP233 );
		}
		label71:;
		label70:;
		int32 vr99 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"add", 4ll );
		if( (int64)vr99 != 0ll ) goto label73;
		{
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			ADD( &SIGN1 );
		}
		label73:;
		label72:;
		int32 vr105 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"push", 5ll );
		if( (int64)vr105 != 0ll ) goto label75;
		{
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			PUSH( &SIGN1 );
		}
		label75:;
		label74:;
		int32 vr111 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"pop", 4ll );
		if( (int64)vr111 != 0ll ) goto label77;
		{
			POP(  );
		}
		label77:;
		label76:;
		int32 vr114 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"mov", 4ll );
		if( (int64)vr114 != 0ll ) goto label79;
		{
			ylSTRING TMP283;
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			__builtin_memset( &TMP283, 0, 24ll );
			ylSTRING* vr119 = TOP(  );
			yl_StrAssign( (void*)&TMP283, -1ll, (void*)vr119, -1ll, 0 );
			MOV( &SIGN1, &TMP283 );
			yl_StrDelete( (ylSTRING*)&TMP283 );
		}
		label79:;
		label78:;
		int32 vr125 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"end", 4ll );
		if( (int64)vr125 != 0ll ) goto label81;
		{
			yl_End( 0 );
		}
		label81:;
		label80:;
		int32 vr128 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"loop", 5ll );
		if( (int64)vr128 != 0ll ) goto label83;
		{
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			ylSTRING* vr132 = TOP(  );
			double vr133 = yl_VAL( (ylSTRING*)vr132 );
			I1 = yl_D2I( vr133 );
			int64 vr136 = yl_FileLocation( (int32)F1 );
			F21 = vr136;
			if( (int64)I1 == 0ll ) goto label85;
			{
				label86:;
				if( ((int64)-((int64)I1 != 0ll) & (int64)-((int64)CONF != 1ll)) == 0ll ) goto label87;
				{
					ylSTRING TMP325;
					ylSTRING TMP335;
					ylSTRING TMP355;
					ylSTRING TMP365;
					__builtin_memset( &TMP365, 0, 24ll );
					__builtin_memset( &TMP355, 0, 24ll );
					ylSTRING* vr148 = yl_StrConcat( &TMP355, (void*)"/loop.", 7ll, (void*)&SIGN1, -1ll );
					yl_StrAssign( (void*)&TMP365, -1ll, (void*)vr148, -1ll, 0 );
					__builtin_memset( &TMP335, 0, 24ll );
					__builtin_memset( &TMP325, 0, 24ll );
					ylSTRING* vr155 = yl_StrConcat( &TMP325, (void*)"loop.", 6ll, (void*)&SIGN1, -1ll );
					yl_StrAssign( (void*)&TMP335, -1ll, (void*)vr155, -1ll, 0 );
					HOSTFILE( &MAINFILE, (int32)F1, &TMP335, &TMP365, &F21 );
					yl_StrDelete( (ylSTRING*)&TMP365 );
					yl_StrDelete( (ylSTRING*)&TMP335 );
					if( (int64)I1 < 0ll ) goto label89;
					{
						I1 = (int32)((int64)I1 + -1ll);
					}
					label89:;
					label88:;
					if( (int64)CONF != 1ll ) goto label91;
					{
						label92:;
						{
							ylSTRING TMP387;
							ylSTRING TMP397;
							yl_FileLineInput( (int32)F1, (void*)&SIGN21, -1ll, 0 );
							__builtin_memset( &TMP387, 0, 24ll );
							ylSTRING* vr171 = yl_StrConcat( &TMP387, (void*)"</loop.", 8ll, (void*)&SIGN1, -1ll );
							__builtin_memset( &TMP397, 0, 24ll );
							ylSTRING* vr174 = yl_StrConcat( &TMP397, (void*)vr171, -1ll, (void*)">", 2ll );
							int32 vr176 = yl_StrCompare( (void*)&SIGN21, -1ll, (void*)vr174, -1ll );
							if( (int64)vr176 != 0ll ) goto label96;
							{
								goto label93;
								label96:;
							}
						}
						label94:;
						goto label92;
						label93:;
						goto label87;
					}
					label91:;
					label90:;
				}
				goto label86;
				label87:;
			}
			goto label84;
			label85:;
			{
				label97:;
				{
					ylSTRING TMP405;
					ylSTRING TMP415;
					yl_FileLineInput( (int32)F1, (void*)&SIGN21, -1ll, 0 );
					__builtin_memset( &TMP405, 0, 24ll );
					ylSTRING* vr183 = yl_StrConcat( &TMP405, (void*)"</loop.", 8ll, (void*)&SIGN1, -1ll );
					__builtin_memset( &TMP415, 0, 24ll );
					ylSTRING* vr186 = yl_StrConcat( &TMP415, (void*)vr183, -1ll, (void*)">", 2ll );
					int32 vr188 = yl_StrCompare( (void*)&SIGN21, -1ll, (void*)vr186, -1ll );
					if( (int64)vr188 != 0ll ) goto label101;
					{
						goto label98;
						label101:;
					}
				}
				label99:;
				goto label97;
				label98:;
			}
			label84:;
			CONF = 0;
		}
		label83:;
		label82:;
		int32 vr191 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"logic", 6ll );
		if( (int64)vr191 != 0ll ) goto label103;
		{
			ylSTRING TMP933;
			ylSTRING TMP943;
			yl_FileLineInput( (int32)F1, (void*)&SIGN1, -1ll, 0 );
			double vr195 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr196 = TOP(  );
			double vr197 = yl_VAL( (ylSTRING*)vr196 );
			int32 vr199 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)">", 2ll );
			if( ((int64)-(vr195 > vr197) & (int64)-((int64)vr199 == 0ll)) == 0ll ) goto label105;
			{
				ylSTRING TMP444;
				ylSTRING TMP454;
				ylSTRING TMP474;
				ylSTRING TMP484;
				int64 TMP494;
				int64 vr204 = yl_FileLocation( (int32)F1 );
				TMP494 = vr204;
				__builtin_memset( &TMP484, 0, 24ll );
				__builtin_memset( &TMP474, 0, 24ll );
				ylSTRING* vr210 = yl_StrConcat( &TMP474, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP484, -1ll, (void*)vr210, -1ll, 0 );
				__builtin_memset( &TMP454, 0, 24ll );
				__builtin_memset( &TMP444, 0, 24ll );
				ylSTRING* vr217 = yl_StrConcat( &TMP444, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP454, -1ll, (void*)vr217, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP454, &TMP484, &TMP494 );
				yl_StrDelete( (ylSTRING*)&TMP484 );
				yl_StrDelete( (ylSTRING*)&TMP454 );
				goto label106;
			}
			label105:;
			label104:;
			double vr223 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr224 = TOP(  );
			double vr225 = yl_VAL( (ylSTRING*)vr224 );
			int32 vr227 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)">=", 3ll );
			if( ((int64)-(vr223 >= vr225) & (int64)-((int64)vr227 == 0ll)) == 0ll ) goto label108;
			{
				ylSTRING TMP514;
				ylSTRING TMP524;
				ylSTRING TMP534;
				ylSTRING TMP544;
				int64 TMP554;
				int64 vr232 = yl_FileLocation( (int32)F1 );
				TMP554 = vr232;
				__builtin_memset( &TMP544, 0, 24ll );
				__builtin_memset( &TMP534, 0, 24ll );
				ylSTRING* vr238 = yl_StrConcat( &TMP534, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP544, -1ll, (void*)vr238, -1ll, 0 );
				__builtin_memset( &TMP524, 0, 24ll );
				__builtin_memset( &TMP514, 0, 24ll );
				ylSTRING* vr245 = yl_StrConcat( &TMP514, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP524, -1ll, (void*)vr245, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP524, &TMP544, &TMP554 );
				yl_StrDelete( (ylSTRING*)&TMP544 );
				yl_StrDelete( (ylSTRING*)&TMP524 );
				goto label106;
			}
			label108:;
			label107:;
			double vr251 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr252 = TOP(  );
			double vr253 = yl_VAL( (ylSTRING*)vr252 );
			int32 vr255 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"<", 2ll );
			if( ((int64)-(vr251 < vr253) & (int64)-((int64)vr255 == 0ll)) == 0ll ) goto label110;
			{
				ylSTRING TMP564;
				ylSTRING TMP574;
				ylSTRING TMP584;
				ylSTRING TMP594;
				int64 TMP604;
				int64 vr260 = yl_FileLocation( (int32)F1 );
				TMP604 = vr260;
				__builtin_memset( &TMP594, 0, 24ll );
				__builtin_memset( &TMP584, 0, 24ll );
				ylSTRING* vr266 = yl_StrConcat( &TMP584, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP594, -1ll, (void*)vr266, -1ll, 0 );
				__builtin_memset( &TMP574, 0, 24ll );
				__builtin_memset( &TMP564, 0, 24ll );
				ylSTRING* vr273 = yl_StrConcat( &TMP564, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP574, -1ll, (void*)vr273, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP574, &TMP594, &TMP604 );
				yl_StrDelete( (ylSTRING*)&TMP594 );
				yl_StrDelete( (ylSTRING*)&TMP574 );
				goto label106;
			}
			label110:;
			label109:;
			double vr279 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr280 = TOP(  );
			double vr281 = yl_VAL( (ylSTRING*)vr280 );
			int32 vr283 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"<=", 3ll );
			if( ((int64)-(vr279 <= vr281) & (int64)-((int64)vr283 == 0ll)) == 0ll ) goto label112;
			{
				ylSTRING TMP624;
				ylSTRING TMP634;
				ylSTRING TMP644;
				ylSTRING TMP654;
				int64 TMP664;
				int64 vr288 = yl_FileLocation( (int32)F1 );
				TMP664 = vr288;
				__builtin_memset( &TMP654, 0, 24ll );
				__builtin_memset( &TMP644, 0, 24ll );
				ylSTRING* vr294 = yl_StrConcat( &TMP644, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP654, -1ll, (void*)vr294, -1ll, 0 );
				__builtin_memset( &TMP634, 0, 24ll );
				__builtin_memset( &TMP624, 0, 24ll );
				ylSTRING* vr301 = yl_StrConcat( &TMP624, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP634, -1ll, (void*)vr301, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP634, &TMP654, &TMP664 );
				yl_StrDelete( (ylSTRING*)&TMP654 );
				yl_StrDelete( (ylSTRING*)&TMP634 );
				goto label106;
			}
			label112:;
			label111:;
			double vr307 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr308 = TOP(  );
			double vr309 = yl_VAL( (ylSTRING*)vr308 );
			int32 vr311 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"==", 3ll );
			if( ((int64)-(vr307 == vr309) & (int64)-((int64)vr311 == 0ll)) == 0ll ) goto label114;
			{
				ylSTRING TMP684;
				ylSTRING TMP694;
				ylSTRING TMP704;
				ylSTRING TMP714;
				int64 TMP724;
				int64 vr316 = yl_FileLocation( (int32)F1 );
				TMP724 = vr316;
				__builtin_memset( &TMP714, 0, 24ll );
				__builtin_memset( &TMP704, 0, 24ll );
				ylSTRING* vr322 = yl_StrConcat( &TMP704, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP714, -1ll, (void*)vr322, -1ll, 0 );
				__builtin_memset( &TMP694, 0, 24ll );
				__builtin_memset( &TMP684, 0, 24ll );
				ylSTRING* vr329 = yl_StrConcat( &TMP684, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP694, -1ll, (void*)vr329, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP694, &TMP714, &TMP724 );
				yl_StrDelete( (ylSTRING*)&TMP714 );
				yl_StrDelete( (ylSTRING*)&TMP694 );
				goto label106;
			}
			label114:;
			label113:;
			ylSTRING* vr335 = TOP(  );
			int32 vr336 = yl_StrCompare( (void*)((uint8*)STACK + 72ll), -1ll, (void*)vr335, -1ll );
			int32 vr339 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"=", 2ll );
			if( ((int64)-((int64)vr336 == 0ll) & (int64)-((int64)vr339 == 0ll)) == 0ll ) goto label116;
			{
				ylSTRING TMP744;
				ylSTRING TMP754;
				ylSTRING TMP764;
				ylSTRING TMP774;
				int64 TMP784;
				int64 vr344 = yl_FileLocation( (int32)F1 );
				TMP784 = vr344;
				__builtin_memset( &TMP774, 0, 24ll );
				__builtin_memset( &TMP764, 0, 24ll );
				ylSTRING* vr350 = yl_StrConcat( &TMP764, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP774, -1ll, (void*)vr350, -1ll, 0 );
				__builtin_memset( &TMP754, 0, 24ll );
				__builtin_memset( &TMP744, 0, 24ll );
				ylSTRING* vr357 = yl_StrConcat( &TMP744, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP754, -1ll, (void*)vr357, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP754, &TMP774, &TMP784 );
				yl_StrDelete( (ylSTRING*)&TMP774 );
				yl_StrDelete( (ylSTRING*)&TMP754 );
				goto label106;
			}
			label116:;
			label115:;
			ylSTRING* vr363 = TOP(  );
			int32 vr364 = yl_StrCompare( (void*)((uint8*)STACK + 72ll), -1ll, (void*)vr363, -1ll );
			int32 vr367 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"!=", 3ll );
			if( ((int64)-((int64)vr364 != 0ll) & (int64)-((int64)vr367 == 0ll)) == 0ll ) goto label118;
			{
				ylSTRING TMP804;
				ylSTRING TMP814;
				ylSTRING TMP824;
				ylSTRING TMP834;
				int64 TMP844;
				int64 vr372 = yl_FileLocation( (int32)F1 );
				TMP844 = vr372;
				__builtin_memset( &TMP834, 0, 24ll );
				__builtin_memset( &TMP824, 0, 24ll );
				ylSTRING* vr378 = yl_StrConcat( &TMP824, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP834, -1ll, (void*)vr378, -1ll, 0 );
				__builtin_memset( &TMP814, 0, 24ll );
				__builtin_memset( &TMP804, 0, 24ll );
				ylSTRING* vr385 = yl_StrConcat( &TMP804, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP814, -1ll, (void*)vr385, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP814, &TMP834, &TMP844 );
				yl_StrDelete( (ylSTRING*)&TMP834 );
				yl_StrDelete( (ylSTRING*)&TMP814 );
				goto label106;
			}
			label118:;
			label117:;
			double vr391 = yl_VAL( (ylSTRING*)((uint8*)STACK + 72ll) );
			ylSTRING* vr392 = TOP(  );
			double vr393 = yl_VAL( (ylSTRING*)vr392 );
			int32 vr395 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"!==", 4ll );
			if( ((int64)-(vr391 == vr393) & (int64)-((int64)vr395 == 0ll)) == 0ll ) goto label120;
			{
				ylSTRING TMP864;
				ylSTRING TMP874;
				ylSTRING TMP884;
				ylSTRING TMP894;
				int64 TMP904;
				int64 vr400 = yl_FileLocation( (int32)F1 );
				TMP904 = vr400;
				__builtin_memset( &TMP894, 0, 24ll );
				__builtin_memset( &TMP884, 0, 24ll );
				ylSTRING* vr406 = yl_StrConcat( &TMP884, (void*)"/logic.", 8ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP894, -1ll, (void*)vr406, -1ll, 0 );
				__builtin_memset( &TMP874, 0, 24ll );
				__builtin_memset( &TMP864, 0, 24ll );
				ylSTRING* vr413 = yl_StrConcat( &TMP864, (void*)"logic.", 7ll, (void*)&SIGN1, -1ll );
				yl_StrAssign( (void*)&TMP874, -1ll, (void*)vr413, -1ll, 0 );
				HOSTFILE( &MAINFILE, (int32)F1, &TMP874, &TMP894, &TMP904 );
				yl_StrDelete( (ylSTRING*)&TMP894 );
				yl_StrDelete( (ylSTRING*)&TMP874 );
				goto label106;
			}
			label120:;
			label119:;
			yl_StrAssign( (void*)&SIGN21, -1ll, (void*)"", 1ll, 0 );
			label121:;
			__builtin_memset( &TMP933, 0, 24ll );
			ylSTRING* vr423 = yl_StrConcat( &TMP933, (void*)"</logic.", 9ll, (void*)&SIGN1, -1ll );
			__builtin_memset( &TMP943, 0, 24ll );
			ylSTRING* vr426 = yl_StrConcat( &TMP943, (void*)vr423, -1ll, (void*)">", 2ll );
			int32 vr428 = yl_StrCompare( (void*)&SIGN21, -1ll, (void*)vr426, -1ll );
			if( (int64)vr428 == 0ll ) goto label122;
			{
				int32 vr430 = yl_ErrorGetNum(  );
				int32 vr434 = yl_FileEof( (int32)F1 );
				if( ((int64)-((int64)vr430 > 0ll) | (int64)-((int64)vr434 != 0ll)) == 0ll ) goto label124;
				{
					ylSTRING TMP955;
					int32 vr438 = yl_ErrorGetNum(  );
					ylSTRING* vr442 = yl_LongintToStr( ((int64)vr438 * 3ll) + 1ll );
					__builtin_memset( &TMP955, 0, 24ll );
					ylSTRING* vr445 = yl_StrConcat( &TMP955, (void*)"Err Unexpected:", 16ll, (void*)vr442, -1ll );
					yl_PrintString( 0, (ylSTRING*)vr445, 1 );
					yl_End( 0 );
				}
				label124:;
				label123:;
				yl_FileLineInput( (int32)F1, (void*)&SIGN21, -1ll, 0 );
			}
			goto label121;
			label122:;
			label106:;
		}
		label103:;
		label102:;
	}
	goto label54;
	label55:;
	yl_StrDelete( (ylSTRING*)&SIGN21 );
	yl_StrDelete( (ylSTRING*)&LABLE1 );
	yl_StrDelete( (ylSTRING*)&SIGN1 );
	label48:;
}

ylSTRING* API( ylSTRING* APINAME1 )
{
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label125:;
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	int32 vr2 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"+", 2ll );
	if( (int64)vr2 != 0ll ) goto label128;
	{
		double vr4 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr5 = TOP(  );
		double vr6 = yl_VAL( (ylSTRING*)vr5 );
		ylSTRING* vr8 = yl_DoubleToStr( vr4 + vr6 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr8, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label128:;
	label127:;
	int32 vr11 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"-", 2ll );
	if( (int64)vr11 != 0ll ) goto label130;
	{
		double vr13 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr14 = TOP(  );
		double vr15 = yl_VAL( (ylSTRING*)vr14 );
		ylSTRING* vr17 = yl_DoubleToStr( vr13 - vr15 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr17, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label130:;
	label129:;
	int32 vr20 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"*", 2ll );
	if( (int64)vr20 != 0ll ) goto label132;
	{
		double vr22 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr23 = TOP(  );
		double vr24 = yl_VAL( (ylSTRING*)vr23 );
		ylSTRING* vr26 = yl_DoubleToStr( vr22 * vr24 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr26, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label132:;
	label131:;
	int32 vr29 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"/", 2ll );
	if( (int64)vr29 != 0ll ) goto label134;
	{
		double vr31 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr32 = TOP(  );
		double vr33 = yl_VAL( (ylSTRING*)vr32 );
		ylSTRING* vr35 = yl_DoubleToStr( vr31 / vr33 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr35, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label134:;
	label133:;
	int32 vr38 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"&", 2ll );
	if( (int64)vr38 != 0ll ) goto label136;
	{
		ylSTRING TMP1012;
		ylSTRING* vr40 = TOP(  );
		__builtin_memset( &TMP1012, 0, 24ll );
		ylSTRING* vr43 = yl_StrConcat( &TMP1012, (void*)((uint8*)STACK + 48ll), -1ll, (void*)vr40, -1ll );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr43, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label136:;
	label135:;
	int32 vr46 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"\x5C", 2ll );
	if( (int64)vr46 != 0ll ) goto label138;
	{
		double vr48 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr50 = TOP(  );
		double vr51 = yl_VAL( (ylSTRING*)vr50 );
		ylSTRING* vr54 = yl_LongintToStr( yl_D2L( vr48 ) % yl_D2L( vr51 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr54, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label138:;
	label137:;
	int32 vr57 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"|", 2ll );
	if( (int64)vr57 != 0ll ) goto label140;
	{
		double vr59 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr60 = TOP(  );
		double vr61 = yl_VAL( (ylSTRING*)vr60 );
		double vr63 = yl_FIXDouble( vr59 / vr61 );
		ylSTRING* vr64 = yl_DoubleToStr( vr63 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr64, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label140:;
	label139:;
	int32 vr67 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"=", 2ll );
	if( (int64)vr67 != 0ll ) goto label142;
	{
		ylSTRING* vr69 = TOP(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr69, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label142:;
	label141:;
	int32 vr72 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.print", 10ll );
	if( (int64)vr72 != 0ll ) goto label144;
	{
		ylSTRING TMP1052;
		__builtin_memset( &TMP1052, 0, 24ll );
		ylSTRING* vr75 = TOP(  );
		yl_StrAssign( (void*)&TMP1052, -1ll, (void*)vr75, -1ll, 0 );
		ylSTRING* vr78 = TRMATE( &TMP1052 );
		yl_PrintString( 0, (ylSTRING*)vr78, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1052 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)"1", 2ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label144:;
	label143:;
	int32 vr82 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.read", 9ll );
	if( (int64)vr82 != 0ll ) goto label146;
	{
		ylSTRING* vr84 = yl_StrAllocTempDescZEx( (uint8*)"", 0ll );
		yl_ConsoleInput( (ylSTRING*)vr84, 0, -1 );
		yl_InputString( (void*)&TMP1, -1ll, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label146:;
	label145:;
	int32 vr89 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.readline", 13ll );
	if( (int64)vr89 != 0ll ) goto label148;
	{
		ylSTRING* vr92 = yl_StrAllocTempDescZEx( (uint8*)"", 0ll );
		yl_LineInput( (ylSTRING*)vr92, (void*)&TMP1, -1ll, 0, 0, -1 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label148:;
	label147:;
	int32 vr96 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.getkey", 11ll );
	if( (int64)vr96 != 0ll ) goto label150;
	{
		int32 vr98 = yl_Getkey(  );
		ylSTRING* vr99 = yl_IntToStr( vr98 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr99, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label150:;
	label149:;
	int32 vr102 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.locate", 11ll );
	if( (int64)vr102 != 0ll ) goto label152;
	{
		ylSTRING* vr104 = TOP(  );
		double vr105 = yl_VAL( (ylSTRING*)vr104 );
		double vr107 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		yl_Locate( yl_D2I( vr107 ), yl_D2I( vr105 ), -1, 0, 0 );
	}
	label152:;
	label151:;
	int32 vr109 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.readkey", 12ll );
	if( (int64)vr109 != 0ll ) goto label154;
	{
		ylSTRING* vr111 = yl_Inkey(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr111, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label154:;
	label153:;
	int32 vr114 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.cls", 8ll );
	if( (int64)vr114 != 0ll ) goto label156;
	{
		yl_Cls( -65536 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)"1", 2ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label156:;
	label155:;
	int32 vr118 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.viewspace", 14ll );
	if( (int64)vr118 != 0ll ) goto label158;
	{
		ylSTRING* vr120 = TOP(  );
		int64 vr121 = yl_VALLNG( (ylSTRING*)vr120 );
		int64 vr123 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		yl_ConsoleView( (int32)vr123, (int32)vr121 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)"1", 2ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label158:;
	label157:;
	int32 vr127 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.color", 10ll );
	if( (int64)vr127 != 0ll ) goto label160;
	{
		int64 vr129 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 72ll) );
		int64 vr134 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		int64 vr140 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 24ll) );
		int64 vr147 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 144ll) );
		int64 vr152 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 120ll) );
		int64 vr158 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 96ll) );
		yl_Color( (int32)(uint32)(((((int64)(uint32)(uint8)vr147 << (16ll & 63ll)) | ((int64)(uint32)(uint8)vr152 << (8ll & 63ll))) | (int64)(uint32)(uint8)vr158) | 4278190080ll), (int32)(uint32)(((((int64)(uint32)(uint8)vr129 << (16ll & 63ll)) | ((int64)(uint32)(uint8)vr134 << (8ll & 63ll))) | (int64)(uint32)(uint8)vr140) | 4278190080ll), 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)"1", 2ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label160:;
	label159:;
	int32 vr167 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.csrlin", 11ll );
	if( (int64)vr167 != 0ll ) goto label162;
	{
		int32 vr169 = yl_GetY(  );
		ylSTRING* vr170 = yl_IntToStr( vr169 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr170, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label162:;
	label161:;
	int32 vr173 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.pos", 8ll );
	if( (int64)vr173 != 0ll ) goto label164;
	{
		int32 vr175 = yl_GetX(  );
		ylSTRING* vr176 = yl_IntToStr( vr175 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr176, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label164:;
	label163:;
	int32 vr179 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"cli.pause", 10ll );
	if( (int64)vr179 != 0ll ) goto label166;
	{
		yl_SleepEx( -1, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)"1", 2ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label166:;
	label165:;
	int32 vr183 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"date.date", 10ll );
	if( (int64)vr183 != 0ll ) goto label168;
	{
		ylSTRING* vr185 = yl_Date(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr185, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label168:;
	label167:;
	int32 vr188 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"date.time", 10ll );
	if( (int64)vr188 != 0ll ) goto label170;
	{
		ylSTRING* vr190 = yl_Time(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr190, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label170:;
	label169:;
	int32 vr193 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"date.setdate", 13ll );
	if( (int64)vr193 != 0ll ) goto label172;
	{
		ylSTRING* vr195 = TOP(  );
		int32 vr196 = yl_SetDate( (ylSTRING*)vr195 );
		ylSTRING* vr197 = yl_IntToStr( vr196 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr197, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label172:;
	label171:;
	int32 vr200 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"date.settime", 13ll );
	if( (int64)vr200 != 0ll ) goto label174;
	{
		ylSTRING* vr202 = TOP(  );
		int32 vr203 = yl_SetTime( (ylSTRING*)vr202 );
		ylSTRING* vr204 = yl_IntToStr( vr203 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr204, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label174:;
	label173:;
	int32 vr207 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"date.timer", 11ll );
	if( (int64)vr207 != 0ll ) goto label176;
	{
		double vr209 = yl_Timer(  );
		ylSTRING* vr210 = yl_DoubleToStr( vr209 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr210, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label176:;
	label175:;
	int32 vr213 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.free", 10ll );
	if( (int64)vr213 != 0ll ) goto label178;
	{
		int32 vr215 = yl_FileFree(  );
		ylSTRING* vr216 = yl_IntToStr( vr215 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr216, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label178:;
	label177:;
	int32 vr219 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.open", 10ll );
	if( (int64)vr219 != 0ll ) goto label180;
	{
		int32 vr221 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"input", 6ll );
		if( (int64)vr221 != 0ll ) goto label182;
		{
			ylSTRING* vr223 = TOP(  );
			int64 vr224 = yl_VALLNG( (ylSTRING*)vr223 );
			yl_FileOpenEncod( (ylSTRING*)((uint8*)STACK + 72ll), 2u, 0u, 0u, (int32)vr224, 0, (uint8*)"ascii" );
		}
		label182:;
		label181:;
		int32 vr226 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"output", 7ll );
		if( (int64)vr226 != 0ll ) goto label184;
		{
			ylSTRING* vr228 = TOP(  );
			int64 vr229 = yl_VALLNG( (ylSTRING*)vr228 );
			yl_FileOpenEncod( (ylSTRING*)((uint8*)STACK + 72ll), 3u, 0u, 0u, (int32)vr229, 0, (uint8*)"ascii" );
		}
		label184:;
		label183:;
		int32 vr231 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"append", 7ll );
		if( (int64)vr231 != 0ll ) goto label186;
		{
			ylSTRING* vr233 = TOP(  );
			int64 vr234 = yl_VALLNG( (ylSTRING*)vr233 );
			yl_FileOpenEncod( (ylSTRING*)((uint8*)STACK + 72ll), 4u, 0u, 0u, (int32)vr234, 0, (uint8*)"ascii" );
		}
		label186:;
		label185:;
		int32 vr236 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"random", 7ll );
		if( (int64)vr236 != 0ll ) goto label188;
		{
			ylSTRING* vr238 = TOP(  );
			int64 vr239 = yl_VALLNG( (ylSTRING*)vr238 );
			yl_FileOpen( (ylSTRING*)((uint8*)STACK + 72ll), 1u, 0u, 0u, (int32)vr239, 0 );
		}
		label188:;
		label187:;
		int32 vr241 = yl_StrCompare( (void*)((uint8*)STACK + 48ll), -1ll, (void*)"binary", 7ll );
		if( (int64)vr241 != 0ll ) goto label190;
		{
			ylSTRING* vr243 = TOP(  );
			int64 vr244 = yl_VALLNG( (ylSTRING*)vr243 );
			yl_FileOpen( (ylSTRING*)((uint8*)STACK + 72ll), 0u, 0u, 0u, (int32)vr244, 0 );
		}
		label190:;
		label189:;
	}
	label180:;
	label179:;
	int32 vr246 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.close", 11ll );
	if( (int64)vr246 != 0ll ) goto label192;
	{
		ylSTRING* vr248 = TOP(  );
		int64 vr249 = yl_VALLNG( (ylSTRING*)vr248 );
		yl_FileClose( (int32)vr249 );
	}
	label192:;
	label191:;
	int32 vr251 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.input", 11ll );
	if( (int64)vr251 != 0ll ) goto label194;
	{
		int64 vr253 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 24ll) );
		yl_FileInput( (int32)vr253 );
		yl_InputString( (void*)&TMP1, -1ll, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label194:;
	label193:;
	int32 vr259 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.lineinput", 15ll );
	if( (int64)vr259 != 0ll ) goto label196;
	{
		ylSTRING* vr262 = TOP(  );
		int64 vr263 = yl_VALLNG( (ylSTRING*)vr262 );
		yl_FileLineInput( (int32)vr263, (void*)&TMP1, -1ll, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label196:;
	label195:;
	int32 vr268 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.print", 11ll );
	if( (int64)vr268 != 0ll ) goto label198;
	{
		int64 TMP1342;
		int64 vr270 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		TMP1342 = vr270;
		ylSTRING* vr271 = TOP(  );
		yl_PrintString( (int32)TMP1342, (ylSTRING*)vr271, 1 );
	}
	label198:;
	label197:;
	int32 vr273 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.put", 9ll );
	if( (int64)vr273 != 0ll ) goto label200;
	{
		ylSTRING* vr275 = TOP(  );
		int64 vr276 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		int64 vr277 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 72ll) );
		yl_FilePutStrLarge( (int32)vr277, vr276, (void*)vr275, -1ll );
	}
	label200:;
	label199:;
	int32 vr279 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.get", 9ll );
	if( (int64)vr279 != 0ll ) goto label202;
	{
		int64 vr282 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		int64 vr283 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 72ll) );
		yl_FileGetStrLarge( (int32)vr283, vr282, (void*)&TMP1, -1ll );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label202:;
	label201:;
	int32 vr288 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.lof", 9ll );
	if( (int64)vr288 != 0ll ) goto label204;
	{
		ylSTRING* vr290 = TOP(  );
		int64 vr291 = yl_VALLNG( (ylSTRING*)vr290 );
		int64 vr293 = yl_FileSize( (int32)vr291 );
		ylSTRING* vr294 = yl_LongintToStr( vr293 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr294, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label204:;
	label203:;
	int32 vr297 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.loc", 9ll );
	if( (int64)vr297 != 0ll ) goto label206;
	{
		ylSTRING* vr299 = TOP(  );
		int64 vr300 = yl_VALLNG( (ylSTRING*)vr299 );
		int64 vr302 = yl_FileLocation( (int32)vr300 );
		ylSTRING* vr303 = yl_LongintToStr( vr302 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr303, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label206:;
	label205:;
	int32 vr306 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.eof", 9ll );
	if( (int64)vr306 != 0ll ) goto label208;
	{
		ylSTRING* vr308 = TOP(  );
		int64 vr309 = yl_VALLNG( (ylSTRING*)vr308 );
		int32 vr311 = yl_FileEof( (int32)vr309 );
		ylSTRING* vr312 = yl_IntToStr( vr311 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr312, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label208:;
	label207:;
	int32 vr315 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.getseek", 13ll );
	if( (int64)vr315 != 0ll ) goto label210;
	{
		ylSTRING* vr317 = TOP(  );
		int64 vr318 = yl_VALLNG( (ylSTRING*)vr317 );
		int64 vr320 = yl_FileTell( (int32)vr318 );
		ylSTRING* vr321 = yl_LongintToStr( vr320 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr321, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label210:;
	label209:;
	int32 vr324 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.setseek", 13ll );
	if( (int64)vr324 != 0ll ) goto label212;
	{
		ylSTRING* vr326 = TOP(  );
		int64 vr327 = yl_VALLNG( (ylSTRING*)vr326 );
		int64 vr328 = yl_VALLNG( (ylSTRING*)((uint8*)STACK + 48ll) );
		yl_FileSeekLarge( (int32)vr328, vr327 );
	}
	label212:;
	label211:;
	int32 vr330 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.kill", 10ll );
	if( (int64)vr330 != 0ll ) goto label214;
	{
		ylSTRING* vr332 = TOP(  );
		yl_FileKill( (ylSTRING*)vr332 );
	}
	label214:;
	label213:;
	int32 vr333 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.rm", 8ll );
	if( (int64)vr333 != 0ll ) goto label216;
	{
		ylSTRING* vr335 = TOP(  );
		yl_FileKill( (ylSTRING*)vr335 );
	}
	label216:;
	label215:;
	int32 vr336 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.mkdir", 11ll );
	if( (int64)vr336 != 0ll ) goto label218;
	{
		ylSTRING* vr338 = TOP(  );
		yl_MkDir( (ylSTRING*)vr338 );
	}
	label218:;
	label217:;
	int32 vr339 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.rmdir", 11ll );
	if( (int64)vr339 != 0ll ) goto label220;
	{
		ylSTRING* vr341 = TOP(  );
		yl_RmDir( (ylSTRING*)vr341 );
	}
	label220:;
	label219:;
	int32 vr342 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"file.rename", 12ll );
	if( (int64)vr342 != 0ll ) goto label222;
	{
		ylSTRING TMP1472;
		__builtin_memset( &TMP1472, 0, 24ll );
		ylSTRING* vr345 = TOP(  );
		yl_StrAssign( (void*)&TMP1472, -1ll, (void*)vr345, -1ll, 0 );
		rename( (uint8*)*(uint8**)((uint8*)STACK + 48ll), (uint8*)*(uint8**)&TMP1472 );
		yl_StrDelete( (ylSTRING*)&TMP1472 );
	}
	label222:;
	label221:;
	int32 vr349 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.abs", 9ll );
	if( (int64)vr349 != 0ll ) goto label224;
	{
		ylSTRING* vr351 = TOP(  );
		double vr352 = yl_VAL( (ylSTRING*)vr351 );
		ylSTRING* vr354 = yl_DoubleToStr( __builtin_fabs( vr352 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr354, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label224:;
	label223:;
	int32 vr357 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.log", 9ll );
	if( (int64)vr357 != 0ll ) goto label226;
	{
		ylSTRING* vr359 = TOP(  );
		double vr360 = yl_VAL( (ylSTRING*)vr359 );
		ylSTRING* vr362 = yl_DoubleToStr( __builtin_log( vr360 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr362, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label226:;
	label225:;
	int32 vr365 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.sqr", 9ll );
	if( (int64)vr365 != 0ll ) goto label228;
	{
		ylSTRING* vr367 = TOP(  );
		double vr368 = yl_VAL( (ylSTRING*)vr367 );
		ylSTRING* vr370 = yl_DoubleToStr( __builtin_sqrt( vr368 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr370, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label228:;
	label227:;
	int32 vr373 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.fix", 9ll );
	if( (int64)vr373 != 0ll ) goto label230;
	{
		ylSTRING* vr375 = TOP(  );
		double vr376 = yl_VAL( (ylSTRING*)vr375 );
		double vr377 = yl_FIXDouble( vr376 );
		ylSTRING* vr378 = yl_DoubleToStr( vr377 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr378, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label230:;
	label229:;
	int32 vr381 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.frac", 10ll );
	if( (int64)vr381 != 0ll ) goto label232;
	{
		ylSTRING* vr383 = TOP(  );
		double vr384 = yl_VAL( (ylSTRING*)vr383 );
		double vr385 = yl_FRACd( vr384 );
		ylSTRING* vr386 = yl_DoubleToStr( vr385 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr386, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label232:;
	label231:;
	int32 vr389 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.int", 9ll );
	if( (int64)vr389 != 0ll ) goto label234;
	{
		ylSTRING* vr391 = TOP(  );
		double vr392 = yl_VAL( (ylSTRING*)vr391 );
		ylSTRING* vr394 = yl_DoubleToStr( __builtin_floor( vr392 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr394, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label234:;
	label233:;
	int32 vr397 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.sgn", 9ll );
	if( (int64)vr397 != 0ll ) goto label236;
	{
		ylSTRING* vr399 = TOP(  );
		double vr400 = yl_VAL( (ylSTRING*)vr399 );
		int32 vr401 = yl_SGNDouble( vr400 );
		ylSTRING* vr402 = yl_IntToStr( vr401 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr402, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label236:;
	label235:;
	int32 vr405 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.sin", 9ll );
	if( (int64)vr405 != 0ll ) goto label238;
	{
		ylSTRING* vr407 = TOP(  );
		double vr408 = yl_VAL( (ylSTRING*)vr407 );
		ylSTRING* vr410 = yl_DoubleToStr( __builtin_sin( vr408 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr410, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label238:;
	label237:;
	int32 vr413 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.asin", 10ll );
	if( (int64)vr413 != 0ll ) goto label240;
	{
		ylSTRING* vr415 = TOP(  );
		double vr416 = yl_VAL( (ylSTRING*)vr415 );
		ylSTRING* vr418 = yl_DoubleToStr( __builtin_asin( vr416 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr418, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label240:;
	label239:;
	int32 vr421 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.cos", 9ll );
	if( (int64)vr421 != 0ll ) goto label242;
	{
		ylSTRING* vr423 = TOP(  );
		double vr424 = yl_VAL( (ylSTRING*)vr423 );
		ylSTRING* vr426 = yl_DoubleToStr( __builtin_cos( vr424 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr426, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label242:;
	label241:;
	int32 vr429 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.acos", 10ll );
	if( (int64)vr429 != 0ll ) goto label244;
	{
		ylSTRING* vr431 = TOP(  );
		double vr432 = yl_VAL( (ylSTRING*)vr431 );
		ylSTRING* vr434 = yl_DoubleToStr( __builtin_acos( vr432 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr434, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label244:;
	label243:;
	int32 vr437 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.tan", 9ll );
	if( (int64)vr437 != 0ll ) goto label246;
	{
		ylSTRING* vr439 = TOP(  );
		double vr440 = yl_VAL( (ylSTRING*)vr439 );
		ylSTRING* vr442 = yl_DoubleToStr( __builtin_tan( vr440 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr442, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label246:;
	label245:;
	int32 vr445 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.atan", 10ll );
	if( (int64)vr445 != 0ll ) goto label248;
	{
		ylSTRING* vr447 = TOP(  );
		double vr448 = yl_VAL( (ylSTRING*)vr447 );
		ylSTRING* vr450 = yl_DoubleToStr( __builtin_atan( vr448 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr450, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label248:;
	label247:;
	int32 vr453 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.bin", 9ll );
	if( (int64)vr453 != 0ll ) goto label250;
	{
		ylSTRING* vr455 = TOP(  );
		int64 vr456 = yl_VALLNG( (ylSTRING*)vr455 );
		ylSTRING* vr457 = yl_BIN_l( (uint64)vr456 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr457, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label250:;
	label249:;
	int32 vr460 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"math.hex", 9ll );
	if( (int64)vr460 != 0ll ) goto label252;
	{
		ylSTRING* vr462 = TOP(  );
		int64 vr463 = yl_VALLNG( (ylSTRING*)vr462 );
		ylSTRING* vr464 = yl_HEX_l( (uint64)vr463 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr464, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label252:;
	label251:;
	int32 vr467 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"ran.new", 8ll );
	if( (int64)vr467 != 0ll ) goto label254;
	{
		double vr469 = yl_Rnd( 0x1.p+0f );
		ylSTRING* vr470 = yl_DoubleToStr( vr469 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr470, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label254:;
	label253:;
	int32 vr473 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"ran.random", 11ll );
	if( (int64)vr473 != 0ll ) goto label256;
	{
		yl_Randomize( -0x1.p+0, 0 );
	}
	label256:;
	label255:;
	int32 vr475 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"ran.newint", 11ll );
	if( (int64)vr475 != 0ll ) goto label258;
	{
		double vr477 = yl_Rnd( 0x1.p+0f );
		double vr479 = yl_FIXDouble( vr477 * 0x1.4p+3 );
		ylSTRING* vr480 = yl_DoubleToStr( vr479 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr480, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label258:;
	label257:;
	int32 vr483 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.exec", 9ll );
	if( (int64)vr483 != 0ll ) goto label260;
	{
		ylSTRING* vr485 = TOP(  );
		yl_Exec( (ylSTRING*)((uint8*)STACK + 48ll), (ylSTRING*)vr485 );
	}
	label260:;
	label259:;
	int32 vr486 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.sleep", 10ll );
	if( (int64)vr486 != 0ll ) goto label262;
	{
		ylSTRING* vr488 = TOP(  );
		int64 vr489 = yl_VALLNG( (ylSTRING*)vr488 );
		yl_SleepEx( (int32)vr489, 1 );
	}
	label262:;
	label261:;
	int32 vr491 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.curdir", 11ll );
	if( (int64)vr491 != 0ll ) goto label264;
	{
		ylSTRING* vr493 = yl_CurDir(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr493, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label264:;
	label263:;
	int32 vr496 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.chdir", 10ll );
	if( (int64)vr496 != 0ll ) goto label266;
	{
		ylSTRING* vr498 = TOP(  );
		yl_ChDir( (ylSTRING*)vr498 );
	}
	label266:;
	label265:;
	int32 vr499 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.exepath", 12ll );
	if( (int64)vr499 != 0ll ) goto label268;
	{
		ylSTRING* vr501 = yl_ExePath(  );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr501, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label268:;
	label267:;
	int32 vr504 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.rm", 7ll );
	if( (int64)vr504 != 0ll ) goto label270;
	{
		ylSTRING* vr506 = TOP(  );
		yl_FileKill( (ylSTRING*)vr506 );
	}
	label270:;
	label269:;
	int32 vr507 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.kill", 9ll );
	if( (int64)vr507 != 0ll ) goto label272;
	{
		ylSTRING* vr509 = TOP(  );
		yl_FileKill( (ylSTRING*)vr509 );
	}
	label272:;
	label271:;
	int32 vr510 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.rename", 11ll );
	if( (int64)vr510 != 0ll ) goto label274;
	{
		ylSTRING TMP1742;
		__builtin_memset( &TMP1742, 0, 24ll );
		ylSTRING* vr513 = TOP(  );
		yl_StrAssign( (void*)&TMP1742, -1ll, (void*)vr513, -1ll, 0 );
		rename( (uint8*)*(uint8**)((uint8*)STACK + 48ll), (uint8*)*(uint8**)&TMP1742 );
		yl_StrDelete( (ylSTRING*)&TMP1742 );
	}
	label274:;
	label273:;
	int32 vr517 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.mkdir", 10ll );
	if( (int64)vr517 != 0ll ) goto label276;
	{
		ylSTRING* vr519 = TOP(  );
		yl_MkDir( (ylSTRING*)vr519 );
	}
	label276:;
	label275:;
	int32 vr520 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.rmdir", 10ll );
	if( (int64)vr520 != 0ll ) goto label278;
	{
		ylSTRING* vr522 = TOP(  );
		yl_RmDir( (ylSTRING*)vr522 );
	}
	label278:;
	label277:;
	int32 vr523 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.freeram", 12ll );
	if( (int64)vr523 != 0ll ) goto label280;
	{
		uint64 vr525 = yl_GetMemAvail( 0 );
		ylSTRING* vr526 = yl_ULongintToStr( vr525 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr526, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label280:;
	label279:;
	int32 vr529 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.command", 12ll );
	if( (int64)vr529 != 0ll ) goto label282;
	{
		ylSTRING* vr531 = yl_Command( -1 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr531, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label282:;
	label281:;
	int32 vr534 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.evar", 9ll );
	if( (int64)vr534 != 0ll ) goto label284;
	{
		ylSTRING* vr536 = TOP(  );
		ylSTRING* vr537 = yl_GetEnviron( (ylSTRING*)vr536 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr537, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label284:;
	label283:;
	int32 vr540 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.setevar", 12ll );
	if( (int64)vr540 != 0ll ) goto label286;
	{
		ylSTRING* vr542 = TOP(  );
		yl_SetEnviron( (ylSTRING*)vr542 );
	}
	label286:;
	label285:;
	int32 vr543 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.shell", 10ll );
	if( (int64)vr543 != 0ll ) goto label288;
	{
		ylSTRING* vr545 = TOP(  );
		yl_Shell( (ylSTRING*)vr545 );
	}
	label288:;
	label287:;
	int32 vr546 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"sys.return", 11ll );
	if( (int64)vr546 != 0ll ) goto label290;
	{
		ylSTRING* vr548 = TOP(  );
		int32 vr549 = yl_VALINT( (ylSTRING*)vr548 );
		yl_End( vr549 );
	}
	label290:;
	label289:;
	int32 vr550 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.len", 11ll );
	if( (int64)vr550 != 0ll ) goto label292;
	{
		ylSTRING* vr552 = TOP(  );
		int64 vr553 = yl_StrLen( (void*)vr552, -1ll );
		ylSTRING* vr554 = yl_LongintToStr( vr553 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr554, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label292:;
	label291:;
	int32 vr557 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.asc", 11ll );
	if( (int64)vr557 != 0ll ) goto label294;
	{
		ylSTRING* vr559 = TOP(  );
		uint32 vr560 = yl_ASC( (ylSTRING*)vr559, 1ll );
		ylSTRING* vr561 = yl_UIntToStr( vr560 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr561, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label294:;
	label293:;
	int32 vr564 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.chr", 11ll );
	if( (int64)vr564 != 0ll ) goto label296;
	{
		ylSTRING* vr566 = TOP(  );
		int64 vr567 = yl_VALLNG( (ylSTRING*)vr566 );
		ylSTRING* vr568 = yl_CHR( 1, vr567 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr568, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label296:;
	label295:;
	int32 vr571 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.mid", 11ll );
	if( (int64)vr571 != 0ll ) goto label298;
	{
		ylSTRING* vr573 = TOP(  );
		double vr574 = yl_VAL( (ylSTRING*)vr573 );
		double vr576 = yl_VAL( (ylSTRING*)((uint8*)STACK + 48ll) );
		ylSTRING* vr578 = yl_StrMid( (ylSTRING*)((uint8*)STACK + 72ll), yl_D2L( vr576 ), yl_D2L( vr574 ) );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr578, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label298:;
	label297:;
	int32 vr581 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.lcase", 13ll );
	if( (int64)vr581 != 0ll ) goto label300;
	{
		ylSTRING* vr583 = TOP(  );
		ylSTRING* vr584 = yl_StrLcase2( (ylSTRING*)vr583, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr584, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label300:;
	label299:;
	int32 vr587 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.ucase", 13ll );
	if( (int64)vr587 != 0ll ) goto label302;
	{
		ylSTRING* vr589 = TOP(  );
		ylSTRING* vr590 = yl_StrUcase2( (ylSTRING*)vr589, 0 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr590, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label302:;
	label301:;
	int32 vr593 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.instr", 13ll );
	if( (int64)vr593 != 0ll ) goto label304;
	{
		ylSTRING* vr595 = TOP(  );
		int32 vr596 = yl_VALINT( (ylSTRING*)((uint8*)STACK + 72ll) );
		int64 vr598 = yl_StrInstr( (int64)vr596, (ylSTRING*)((uint8*)STACK + 48ll), (ylSTRING*)vr595 );
		ylSTRING* vr599 = yl_LongintToStr( vr598 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr599, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label304:;
	label303:;
	int32 vr602 = yl_StrCompare( (void*)APINAME1, -1ll, (void*)"string.rinstr", 14ll );
	if( (int64)vr602 != 0ll ) goto label306;
	{
		int32 vr604 = yl_VALINT( (ylSTRING*)((uint8*)STACK + 72ll) );
		int64 vr606 = yl_StrInstrRev( (ylSTRING*)((uint8*)STACK + 48ll), (ylSTRING*)((uint8*)STACK + 24ll), (int64)vr604 );
		ylSTRING* vr607 = yl_LongintToStr( vr606 );
		yl_StrInit( (void*)&ylresult1, -1ll, (void*)vr607, -1ll, 0 );
		yl_StrDelete( (ylSTRING*)&TMP1 );
		goto label126;
	}
	label306:;
	label305:;
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)"", 1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	goto label126;
	yl_StrDelete( (ylSTRING*)&TMP1 );
	label126:;
	ylSTRING* vr614 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr614;
}

ylSTRING* TRMATE( ylSTRING* B1 )
{
	ylSTRING TMP1921;
	ylSTRING TMP1961;
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label307:;
	__builtin_memset( &TMP1961, 0, 24ll );
	yl_StrAssign( (void*)&TMP1961, -1ll, (void*)"\x0D\x0A", 3ll, 0 );
	__builtin_memset( &TMP1921, 0, 24ll );
	yl_StrAssign( (void*)&TMP1921, -1ll, (void*)"\x5Cn", 3ll, 0 );
	ylSTRING* vr7 = REPLACE( B1, &TMP1921, &TMP1961 );
	yl_StrAssign( (void*)&ylresult1, -1ll, (void*)vr7, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP1961 );
	yl_StrDelete( (ylSTRING*)&TMP1921 );
	label308:;
	ylSTRING* vr12 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr12;
}

__attribute__(( constructor )) static void yl_ctor__yuron( void )
{
	label0:;
	int32 FILENUMBER0;
	__builtin_memset( &FILENUMBER0, 0, 4ll );
	int32 I0;
	__builtin_memset( &I0, 0, 4ll );
	ylSTRING SIGN0;
	__builtin_memset( &SIGN0, 0, 24ll );
	ylSTRING FILENAME0;
	__builtin_memset( &FILENAME0, 0, 24ll );
	CONF = 0;
	{
		I0 = 1;
		label312:;
		{
			yl_StrAssign( (void*)((int64)(ylSTRING*)STACK + ((int64)I0 * 24ll)), -1ll, (void*)"", 1ll, 0 );
		}
		label310:;
		I0 = (int32)((int64)I0 + 1ll);
		label309:;
		if( (int64)I0 <= 16ll ) goto label312;
		label311:;
	}
	ylSTRING* vr11 = yl_Command( -1 );
	int32 vr12 = yl_StrCompare( (void*)vr11, -1ll, (void*)"", 1ll );
	if( (int64)vr12 != 0ll ) goto label314;
	{
		ylSTRING* vr14 = yl_StrAllocTempDescZEx( (uint8*)"Yuron developers' collections. ##Version:Jade-dragon 0.3.2##", 60ll );
		yl_PrintString( 0, (ylSTRING*)vr14, 1 );
		ylSTRING* vr15 = yl_StrAllocTempDescZEx( (uint8*)"copyright (c) chen-chaochen", 27ll );
		yl_PrintString( 0, (ylSTRING*)vr15, 1 );
		ylSTRING* vr16 = yl_StrAllocTempDescZEx( (uint8*)"2022/6/10  y/m/d", 16ll );
		yl_PrintString( 0, (ylSTRING*)vr16, 1 );
		yl_End( 0 );
	}
	goto label313;
	label314:;
	{
		ylSTRING TMP2021;
		int64 TMP2041;
		ylSTRING* vr17 = yl_CurDir(  );
		yl_StrAssign( (void*)&FILENAME0, -1ll, (void*)vr17, -1ll, 0 );
		yl_StrConcatAssign( (void*)&FILENAME0, -1ll, (void*)"/", 2ll, 0 );
		ylSTRING* vr20 = yl_Command( -1 );
		yl_StrConcatAssign( (void*)&FILENAME0, -1ll, (void*)vr20, -1ll, 0 );
		yl_StrConcatAssign( (void*)&FILENAME0, -1ll, (void*)".yl_app", 8ll, 0 );
		yl_StrAssign( (void*)&MAINFILE, -1ll, (void*)&FILENAME0, -1ll, 0 );
		__builtin_memset( &TMP2021, 0, 24ll );
		ylSTRING* vr27 = yl_StrConcat( &TMP2021, (void*)&FILENAME0, -1ll, (void*)".conf", 6ll );
		yl_FileOpenEncod( (ylSTRING*)vr27, 2u, 0u, 0u, 1, 0, (uint8*)"ascii" );
		yl_FileLineInput( 1, (void*)&SIGN0, -1ll, 0 );
		int32 vr29 = yl_ErrorGetNum(  );
		int32 vr32 = yl_FileEof( 1 );
		if( ((int64)-((int64)vr29 > 0ll) | (int64)-((int64)vr32 != 0ll)) == 0ll ) goto label316;
		{
			ylSTRING TMP2032;
			int32 vr36 = yl_ErrorGetNum(  );
			ylSTRING* vr40 = yl_LongintToStr( ((int64)vr36 * 3ll) + 1ll );
			__builtin_memset( &TMP2032, 0, 24ll );
			ylSTRING* vr43 = yl_StrConcat( &TMP2032, (void*)"Err Unexpected:", 16ll, (void*)vr40, -1ll );
			yl_PrintString( 0, (ylSTRING*)vr43, 1 );
			yl_End( 0 );
		}
		label316:;
		label315:;
		yl_FileClose( 1 );
		int32 vr44 = yl_FileFree(  );
		FILENUMBER0 = vr44;
		yl_StrAssign( (void*)&MAINENDSIGN, -1ll, (void*)"/", 2ll, 0 );
		yl_StrConcatAssign( (void*)&MAINENDSIGN, -1ll, (void*)&SIGN0, -1ll, 0 );
		yl_FileOpenEncod( (ylSTRING*)&FILENAME0, 2u, 0u, 0u, FILENUMBER0, 0, (uint8*)"ascii" );
		TMP2041 = 1ll;
		HOSTFILE( &FILENAME0, FILENUMBER0, &SIGN0, &MAINENDSIGN, &TMP2041 );
		yl_End( 0 );
	}
	label313:;
	yl_StrDelete( (ylSTRING*)&FILENAME0 );
	yl_StrDelete( (ylSTRING*)&SIGN0 );
	label1:;
}

static const char __attribute__((used, section(".ylctinf"))) __ylctinf[] = "-l\0advapi32";
