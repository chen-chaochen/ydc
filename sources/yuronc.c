/*
yuronc is licensed under Yulong PSL v1.
https://gitee.com/chen-chaochen/yulong-open-source-license/releases/1.0
*/
typedef   signed char       int8;
typedef unsigned char      uint8;
typedef   signed short      int16;
typedef unsigned short     uint16;
typedef   signed int        int32;
typedef unsigned int       uint32;
typedef   signed long long  int64;
typedef unsigned long long uint64;
typedef struct { char *data; int64 len; int64 size; } ylSTRING;
typedef int8 boolean;
#define yl_D2L( value ) ((int64)__builtin_nearbyint( value ))
void* yl_ErrorSetHandler( void* );
int32 yl_ErrorGetNum( void );
int32 yl_FileOpenEncod( ylSTRING*, uint32, uint32, uint32, int32, int32, uint8* );
int32 yl_FileClose( int32 );
int32 yl_FileLineInput( int32, void*, int64, int32 );
int32 yl_FileFree( void );
int32 yl_FileEof( int32 );
int32 yl_FileKill( ylSTRING* );
void yl_PrintString( int32, ylSTRING*, int32 );
ylSTRING* yl_StrInit( void*, int64, void*, int64, int32 );
ylSTRING* yl_StrAssign( void*, int64, void*, int64, int32 );
void yl_StrDelete( ylSTRING* );
ylSTRING* yl_StrConcat( ylSTRING*, void*, int64, void*, int64 );
int32 yl_StrCompare( void*, int64, void*, int64 );
ylSTRING* yl_StrConcatAssign( void*, int64, void*, int64, int32 );
ylSTRING* yl_StrAllocTempResult( ylSTRING* );
ylSTRING* yl_StrAllocTempDescZEx( uint8*, int64 );
ylSTRING* yl_UIntToStr( uint32 );
ylSTRING* yl_LongintToStr( int64 );
ylSTRING* yl_StrMid( ylSTRING*, int64, int64 );
int64 yl_StrLen( void*, int64 );
uint32 yl_ASC( ylSTRING*, int64 );
void yl_End( int32 );
ylSTRING* yl_Command( int32 );
ylSTRING* yl_CurDir( void );
ylSTRING* yl_ExePath( void );
void yl_End( int32 );
static void yl_ctor__yuronc( void ) __attribute__(( constructor ));
void MAIN( void );
void INCLUDELIB( ylSTRING* );
void ERRCATCH( int64 );
ylSTRING* REPLACE( ylSTRING*, ylSTRING*, ylSTRING* );
void LOADLIB( ylSTRING*, ylSTRING* );
ylSTRING* INVAR( ylSTRING* );
void MAKEFILE( int64, int64, ylSTRING*, ylSTRING* );
ylSTRING* AHR( ylSTRING* );
ylSTRING* FINDCLASS( ylSTRING* );
static int64 LINEN;
static int64 ONUM;
static int64 CNUM;
static ylSTRING LIBROOT;
static ylSTRING MAINFILE;
static ylSTRING CLASSLIB;
static ylSTRING OBJECTLIB;
static ylSTRING OUTFILE;

void ERRCATCH( int64 LINEN1 )
{
	ylSTRING TMP41;
	label2:;
	ylSTRING* vr0 = yl_StrAllocTempDescZEx( (uint8*)"Err of Compiler: Err Formate of code", 36ll );
	yl_PrintString( 0, (ylSTRING*)vr0, 1 );
	ylSTRING* vr1 = yl_LongintToStr( LINEN1 );
	__builtin_memset( &TMP41, 0, 24ll );
	ylSTRING* vr4 = yl_StrConcat( &TMP41, (void*)"In line:", 9ll, (void*)vr1, -1ll );
	yl_PrintString( 0, (ylSTRING*)vr4, 1 );
	ylSTRING* vr5 = yl_StrAllocTempDescZEx( (uint8*)"Excepted for sign", 17ll );
	yl_PrintString( 0, (ylSTRING*)vr5, 1 );
	yl_FileKill( (ylSTRING*)&OUTFILE );
	yl_End( 22 );
	label3:;
}

ylSTRING* FINDCLASS( ylSTRING* A1 )
{
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label4:;
	int64 F1;
	__builtin_memset( &F1, 0, 8ll );
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	ylSTRING VA1;
	__builtin_memset( &VA1, 0, 24ll );
	int32 vr4 = yl_FileFree(  );
	F1 = (int64)vr4;
	yl_FileOpenEncod( (ylSTRING*)&CLASSLIB, 2u, 0u, 0u, (int32)F1, 0, (uint8*)"ascii" );
	label6:;
	int32 vr8 = yl_FileEof( (int32)F1 );
	if( (int64)vr8 == -1ll ) goto label7;
	{
		ylSTRING TMP92;
		ylSTRING TMP102;
		yl_FileLineInput( (int32)F1, (void*)&TMP1, -1ll, 0 );
		__builtin_memset( &TMP92, 0, 24ll );
		ylSTRING* vr14 = yl_StrConcat( &TMP92, (void*)"<", 2ll, (void*)A1, -1ll );
		__builtin_memset( &TMP102, 0, 24ll );
		ylSTRING* vr17 = yl_StrConcat( &TMP102, (void*)vr14, -1ll, (void*)">", 2ll );
		int32 vr19 = yl_StrCompare( (void*)&TMP1, -1ll, (void*)vr17, -1ll );
		if( (int64)vr19 != 0ll ) goto label9;
		{
			goto label7;
			label9:;
		}
	}
	goto label6;
	label7:;
	yl_StrAssign( (void*)&VA1, -1ll, (void*)"", 1ll, 0 );
	label10:;
	int32 vr23 = yl_FileEof( (int32)F1 );
	if( (int64)vr23 == -1ll ) goto label11;
	{
		ylSTRING TMP122;
		ylSTRING TMP132;
		ylSTRING TMP152;
		ylSTRING TMP162;
		yl_FileLineInput( (int32)F1, (void*)&TMP1, -1ll, 0 );
		__builtin_memset( &TMP122, 0, 24ll );
		ylSTRING* vr29 = yl_StrConcat( &TMP122, (void*)"</", 3ll, (void*)A1, -1ll );
		__builtin_memset( &TMP132, 0, 24ll );
		ylSTRING* vr32 = yl_StrConcat( &TMP132, (void*)vr29, -1ll, (void*)">", 2ll );
		int32 vr34 = yl_StrCompare( (void*)&TMP1, -1ll, (void*)vr32, -1ll );
		if( (int64)vr34 != 0ll ) goto label13;
		{
			goto label11;
			label13:;
		}
		__builtin_memset( &TMP152, 0, 24ll );
		ylSTRING* vr40 = yl_StrConcat( &TMP152, (void*)&VA1, -1ll, (void*)"\x0A", 2ll );
		__builtin_memset( &TMP162, 0, 24ll );
		ylSTRING* vr43 = yl_StrConcat( &TMP162, (void*)vr40, -1ll, (void*)&TMP1, -1ll );
		yl_StrAssign( (void*)&VA1, -1ll, (void*)vr43, -1ll, 0 );
	}
	goto label10;
	label11:;
	yl_FileClose( (int32)F1 );
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)&VA1, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&VA1 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	goto label5;
	yl_StrDelete( (ylSTRING*)&VA1 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	label5:;
	ylSTRING* vr53 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr53;
}

ylSTRING* REPLACE( ylSTRING* A1, ylSTRING* B1, ylSTRING* C1 )
{
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label14:;
	double I1;
	__builtin_memset( &I1, 0, 8ll );
	ylSTRING REP1;
	__builtin_memset( &REP1, 0, 24ll );
	I1 = 0x1.p+0;
	yl_StrAssign( (void*)&REP1, -1ll, (void*)"", 1ll, 0 );
	label16:;
	{
		int64 vr4 = yl_StrLen( (void*)B1, -1ll );
		ylSTRING* vr6 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), vr4 );
		int32 vr7 = yl_StrCompare( (void*)vr6, -1ll, (void*)B1, -1ll );
		if( (int64)vr7 != 0ll ) goto label20;
		{
			ylSTRING TMP173;
			int64 vr9 = yl_StrLen( (void*)B1, -1ll );
			I1 = (I1 + (double)vr9) + -0x1.p+0;
			__builtin_memset( &TMP173, 0, 24ll );
			ylSTRING* vr16 = yl_StrConcat( &TMP173, (void*)&REP1, -1ll, (void*)C1, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr16, -1ll, 0 );
		}
		goto label19;
		label20:;
		{
			ylSTRING TMP183;
			ylSTRING* vr19 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), 1ll );
			__builtin_memset( &TMP183, 0, 24ll );
			ylSTRING* vr23 = yl_StrConcat( &TMP183, (void*)&REP1, -1ll, (void*)vr19, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr23, -1ll, 0 );
		}
		label19:;
		I1 = I1 + 0x1.p+0;
		int64 vr26 = yl_StrLen( (void*)A1, -1ll );
		ylSTRING* vr30 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), 1ll );
		int32 vr31 = yl_StrCompare( (void*)vr30, -1ll, (void*)B1, -1ll );
		if( ((int64)-(I1 >= (double)vr26) & (int64)-((int64)vr31 != 0ll)) == 0ll ) goto label22;
		{
			ylSTRING TMP193;
			ylSTRING* vr36 = yl_StrMid( (ylSTRING*)A1, yl_D2L( I1 ), 1ll );
			__builtin_memset( &TMP193, 0, 24ll );
			ylSTRING* vr40 = yl_StrConcat( &TMP193, (void*)&REP1, -1ll, (void*)vr36, -1ll );
			yl_StrAssign( (void*)&REP1, -1ll, (void*)vr40, -1ll, 0 );
			goto label17;
		}
		label22:;
		label21:;
	}
	label18:;
	goto label16;
	label17:;
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)&REP1, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&REP1 );
	goto label15;
	yl_StrDelete( (ylSTRING*)&REP1 );
	label15:;
	ylSTRING* vr47 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr47;
}

void MAIN( void )
{
	void* TMP231;
	ylSTRING TMP301;
	ylSTRING TMP311;
	ylSTRING TMP341;
	label23:;
	int64 F11;
	__builtin_memset( &F11, 0, 8ll );
	int64 F21;
	__builtin_memset( &F21, 0, 8ll );
	LINEN = 0ll;
	ONUM = 0ll;
	CNUM = 0ll;
	ylSTRING* vr2 = yl_Command( -1 );
	int32 vr3 = yl_StrCompare( (void*)vr2, -1ll, (void*)"", 1ll );
	if( (int64)vr3 != 0ll ) goto label26;
	{
		ylSTRING* vr5 = yl_StrAllocTempDescZEx( (uint8*)"Yuron developers' collections. ##Version:Jade-dragon 0.3.2##", 60ll );
		yl_PrintString( 0, (ylSTRING*)vr5, 1 );
		ylSTRING* vr6 = yl_StrAllocTempDescZEx( (uint8*)"copyright (c) chen-chaochen", 27ll );
		yl_PrintString( 0, (ylSTRING*)vr6, 1 );
		ylSTRING* vr7 = yl_StrAllocTempDescZEx( (uint8*)"2022/6/10  y/m/d", 16ll );
		yl_PrintString( 0, (ylSTRING*)vr7, 1 );
	}
	label26:;
	label25:;
	void* vr9 = yl_ErrorSetHandler( &&label27 );
	TMP231 = vr9;
	ylSTRING* vr10 = yl_ExePath(  );
	yl_StrAssign( (void*)&LIBROOT, -1ll, (void*)vr10, -1ll, 0 );
	yl_StrConcatAssign( (void*)&LIBROOT, -1ll, (void*)"/libroot/", 10ll, 0 );
	ylSTRING* vr11 = yl_CurDir(  );
	yl_StrAssign( (void*)&OUTFILE, -1ll, (void*)vr11, -1ll, 0 );
	yl_StrConcatAssign( (void*)&OUTFILE, -1ll, (void*)"/", 2ll, 0 );
	ylSTRING* vr12 = yl_Command( -1 );
	yl_StrConcatAssign( (void*)&OUTFILE, -1ll, (void*)vr12, -1ll, 0 );
	yl_StrAssign( (void*)&MAINFILE, -1ll, (void*)&OUTFILE, -1ll, 0 );
	yl_StrConcatAssign( (void*)&MAINFILE, -1ll, (void*)".yuron", 7ll, 0 );
	yl_StrConcatAssign( (void*)&OUTFILE, -1ll, (void*)".yl_app", 8ll, 0 );
	ylSTRING* vr13 = yl_ExePath(  );
	yl_StrAssign( (void*)&CLASSLIB, -1ll, (void*)vr13, -1ll, 0 );
	yl_StrConcatAssign( (void*)&CLASSLIB, -1ll, (void*)"/tmp/classlib.tmp", 18ll, 0 );
	ylSTRING* vr14 = yl_ExePath(  );
	yl_StrAssign( (void*)&OBJECTLIB, -1ll, (void*)vr14, -1ll, 0 );
	yl_StrConcatAssign( (void*)&OBJECTLIB, -1ll, (void*)"/tmp/objectlib.tmp", 19ll, 0 );
	yl_FileOpenEncod( (ylSTRING*)&CLASSLIB, 3u, 0u, 0u, 1, 0, (uint8*)"ascii" );
	yl_FileClose( 1 );
	yl_FileOpenEncod( (ylSTRING*)&OBJECTLIB, 3u, 0u, 0u, 1, 0, (uint8*)"ascii" );
	yl_FileClose( 1 );
	int32 vr15 = yl_FileFree(  );
	F11 = (int64)vr15;
	yl_FileOpenEncod( (ylSTRING*)&MAINFILE, 2u, 0u, 0u, (int32)F11, 0, (uint8*)"ascii" );
	int32 vr18 = yl_FileFree(  );
	F21 = (int64)vr18;
	yl_FileOpenEncod( (ylSTRING*)&OUTFILE, 3u, 0u, 0u, (int32)F21, 0, (uint8*)"ascii" );
	__builtin_memset( &TMP311, 0, 24ll );
	yl_StrAssign( (void*)&TMP311, -1ll, (void*)"", 1ll, 0 );
	__builtin_memset( &TMP301, 0, 24ll );
	yl_StrAssign( (void*)&TMP301, -1ll, (void*)"", 1ll, 0 );
	MAKEFILE( F11, F21, &TMP301, &TMP311 );
	yl_StrDelete( (ylSTRING*)&TMP311 );
	yl_StrDelete( (ylSTRING*)&TMP301 );
	yl_FileClose( (int32)F11 );
	yl_FileClose( (int32)F21 );
	if( ONUM == CNUM ) goto label29;
	{
		yl_FileKill( (ylSTRING*)&OUTFILE );
		ylSTRING* vr31 = yl_StrAllocTempDescZEx( (uint8*)"Err of Compiler:  Excepted for '{' or '}'", 41ll );
		yl_PrintString( 0, (ylSTRING*)vr31, 1 );
		yl_End( 32 );
	}
	label29:;
	label28:;
	yl_FileKill( (ylSTRING*)&CLASSLIB );
	yl_FileKill( (ylSTRING*)&OBJECTLIB );
	yl_End( 0 );
	label27:;
	int32 vr32 = yl_ErrorGetNum(  );
	ylSTRING* vr36 = yl_LongintToStr( ((int64)vr32 * 3ll) + 1ll );
	__builtin_memset( &TMP341, 0, 24ll );
	ylSTRING* vr39 = yl_StrConcat( &TMP341, (void*)"error:", 7ll, (void*)vr36, -1ll );
	yl_PrintString( 0, (ylSTRING*)vr39, 1 );
	int32 vr40 = yl_ErrorGetNum(  );
	yl_End( (int32)(((int64)vr40 * 3ll) + 1ll) );
	label24:;
	yl_ErrorSetHandler( TMP231 );
}

ylSTRING* AHR( ylSTRING* B1 )
{
	ylSTRING TMP371;
	ylSTRING TMP391;
	ylSTRING TMP401;
	ylSTRING TMP411;
	ylSTRING TMP431;
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label30:;
	int64 I1;
	__builtin_memset( &I1, 0, 8ll );
	ylSTRING A1;
	__builtin_memset( &A1, 0, 24ll );
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	yl_StrAssign( (void*)&TMP1, -1ll, (void*)"", 1ll, 0 );
	__builtin_memset( &TMP431, 0, 24ll );
	yl_StrAssign( (void*)&TMP431, -1ll, (void*)"[this]", 7ll, 0 );
	__builtin_memset( &TMP411, 0, 24ll );
	__builtin_memset( &TMP371, 0, 24ll );
	yl_StrAssign( (void*)&TMP371, -1ll, (void*)"this", 5ll, 0 );
	ylSTRING* vr12 = INVAR( &TMP371 );
	__builtin_memset( &TMP391, 0, 24ll );
	ylSTRING* vr15 = yl_StrConcat( &TMP391, (void*)"[", 2ll, (void*)vr12, -1ll );
	__builtin_memset( &TMP401, 0, 24ll );
	ylSTRING* vr18 = yl_StrConcat( &TMP401, (void*)vr15, -1ll, (void*)"]", 2ll );
	yl_StrAssign( (void*)&TMP411, -1ll, (void*)vr18, -1ll, 0 );
	ylSTRING* vr21 = REPLACE( B1, &TMP411, &TMP431 );
	yl_StrAssign( (void*)&A1, -1ll, (void*)vr21, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP431 );
	yl_StrDelete( (ylSTRING*)&TMP411 );
	yl_StrDelete( (ylSTRING*)&TMP371 );
	{
		I1 = 1ll;
		int64 TMP442;
		int64 vr27 = yl_StrLen( (void*)&A1, -1ll );
		TMP442 = vr27;
		goto label32;
		label35:;
		{
			ylSTRING* vr29 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
			int32 vr30 = yl_StrCompare( (void*)vr29, -1ll, (void*)"[", 2ll );
			ylSTRING* vr34 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
			int32 vr35 = yl_StrCompare( (void*)vr34, -1ll, (void*)"]", 2ll );
			ylSTRING* vr40 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
			int32 vr41 = yl_StrCompare( (void*)vr40, -1ll, (void*)"\x0A", 2ll );
			ylSTRING* vr45 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
			int32 vr46 = yl_StrCompare( (void*)vr45, -1ll, (void*)"\x0D", 2ll );
			if( (((int64)-((int64)vr30 == 0ll) | (int64)-((int64)vr35 == 0ll)) | ((int64)-((int64)vr41 == 0ll) | (int64)-((int64)vr46 == 0ll))) == 0ll ) goto label37;
			{
				ylSTRING* vr52 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
				yl_StrConcatAssign( (void*)&TMP1, -1ll, (void*)vr52, -1ll, 0 );
			}
			goto label36;
			label37:;
			{
				ylSTRING* vr55 = yl_StrMid( (ylSTRING*)&A1, I1, 1ll );
				uint32 vr56 = yl_ASC( (ylSTRING*)vr55, 1ll );
				ylSTRING* vr57 = yl_UIntToStr( vr56 );
				yl_StrConcatAssign( (void*)&TMP1, -1ll, (void*)vr57, -1ll, 0 );
			}
			label36:;
		}
		label33:;
		I1 = I1 + 1ll;
		label32:;
		if( I1 <= TMP442 ) goto label35;
		label34:;
	}
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	yl_StrDelete( (ylSTRING*)&A1 );
	goto label31;
	yl_StrDelete( (ylSTRING*)&TMP1 );
	yl_StrDelete( (ylSTRING*)&A1 );
	label31:;
	ylSTRING* vr67 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr67;
}

void INCLUDELIB( ylSTRING* LIBNAME1 )
{
	ylSTRING TMP471;
	ylSTRING TMP481;
	ylSTRING TMP491;
	label38:;
	ylSTRING LIBFILE1;
	__builtin_memset( &LIBFILE1, 0, 24ll );
	int64 F11;
	__builtin_memset( &F11, 0, 8ll );
	int64 F21;
	__builtin_memset( &F21, 0, 8ll );
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	__builtin_memset( &TMP481, 0, 24ll );
	yl_StrAssign( (void*)&TMP481, -1ll, (void*)"", 1ll, 0 );
	__builtin_memset( &TMP471, 0, 24ll );
	yl_StrAssign( (void*)&TMP471, -1ll, (void*)" ", 2ll, 0 );
	ylSTRING* vr10 = REPLACE( LIBNAME1, &TMP471, &TMP481 );
	__builtin_memset( &TMP491, 0, 24ll );
	ylSTRING* vr13 = yl_StrConcat( &TMP491, (void*)&LIBROOT, -1ll, (void*)vr10, -1ll );
	yl_StrAssign( (void*)&LIBFILE1, -1ll, (void*)vr13, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP481 );
	yl_StrDelete( (ylSTRING*)&TMP471 );
	int32 vr17 = yl_FileFree(  );
	F11 = (int64)vr17;
	yl_FileOpenEncod( (ylSTRING*)&LIBFILE1, 2u, 0u, 0u, (int32)F11, 0, (uint8*)"ascii" );
	int32 vr21 = yl_FileFree(  );
	F21 = (int64)vr21;
	yl_FileOpenEncod( (ylSTRING*)&CLASSLIB, 4u, 0u, 0u, (int32)F21, 0, (uint8*)"ascii" );
	label40:;
	int32 vr25 = yl_FileEof( (int32)F11 );
	if( (int64)vr25 == -1ll ) goto label41;
	{
		int64 TMP502;
		yl_FileLineInput( (int32)F11, (void*)&TMP1, -1ll, 0 );
		TMP502 = F21;
		yl_PrintString( (int32)TMP502, (ylSTRING*)&TMP1, 1 );
	}
	goto label40;
	label41:;
	yl_FileClose( (int32)F11 );
	yl_FileClose( (int32)F21 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	yl_StrDelete( (ylSTRING*)&LIBFILE1 );
	label39:;
}

void LOADLIB( ylSTRING* LIBNAME1, ylSTRING* OBNAME1 )
{
	void* TMP511;
	ylSTRING TMP721;
	label42:;
	int64 F11;
	__builtin_memset( &F11, 0, 8ll );
	int64 F21;
	__builtin_memset( &F21, 0, 8ll );
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	void* vr4 = yl_ErrorSetHandler( &&label44 );
	TMP511 = vr4;
	int32 vr5 = yl_FileFree(  );
	F11 = (int64)vr5;
	yl_FileOpenEncod( (ylSTRING*)&CLASSLIB, 2u, 0u, 0u, (int32)F11, 0, (uint8*)"ascii" );
	int32 vr8 = yl_FileFree(  );
	F21 = (int64)vr8;
	yl_FileOpenEncod( (ylSTRING*)&OBJECTLIB, 4u, 0u, 0u, (int32)F21, 0, (uint8*)"ascii" );
	label45:;
	int32 vr12 = yl_FileEof( (int32)F11 );
	if( (int64)vr12 != 0ll ) goto label46;
	{
		ylSTRING TMP522;
		ylSTRING TMP532;
		yl_FileLineInput( (int32)F11, (void*)&TMP1, -1ll, 0 );
		__builtin_memset( &TMP522, 0, 24ll );
		ylSTRING* vr18 = yl_StrConcat( &TMP522, (void*)"<", 2ll, (void*)OBNAME1, -1ll );
		__builtin_memset( &TMP532, 0, 24ll );
		ylSTRING* vr21 = yl_StrConcat( &TMP532, (void*)vr18, -1ll, (void*)">", 2ll );
		int32 vr23 = yl_StrCompare( (void*)&TMP1, -1ll, (void*)vr21, -1ll );
		if( (int64)vr23 != 0ll ) goto label48;
		{
			goto label46;
			ylSTRING* vr25 = yl_StrAllocTempDescZEx( (uint8*)"1", 1ll );
			yl_PrintString( 0, (ylSTRING*)vr25, 1 );
		}
		label48:;
		label47:;
	}
	goto label45;
	label46:;
	label49:;
	int32 vr27 = yl_FileEof( (int32)F11 );
	if( (int64)vr27 == -1ll ) goto label50;
	{
		ylSTRING TMP552;
		ylSTRING TMP562;
		int64 TMP572;
		ylSTRING TMP712;
		yl_FileLineInput( (int32)F11, (void*)&TMP1, -1ll, 0 );
		__builtin_memset( &TMP552, 0, 24ll );
		ylSTRING* vr33 = yl_StrConcat( &TMP552, (void*)"</", 3ll, (void*)OBNAME1, -1ll );
		__builtin_memset( &TMP562, 0, 24ll );
		ylSTRING* vr36 = yl_StrConcat( &TMP562, (void*)vr33, -1ll, (void*)">", 2ll );
		int32 vr38 = yl_StrCompare( (void*)&TMP1, -1ll, (void*)vr36, -1ll );
		if( (int64)vr38 != 0ll ) goto label52;
		{
			goto label50;
			label52:;
		}
		TMP572 = F21;
		__builtin_memset( &TMP712, 0, 24ll );
		yl_StrAssign( (void*)&TMP712, -1ll, (void*)"[116104105115]", 15ll, 0 );
		ylSTRING* vr44 = REPLACE( &TMP1, &TMP712, LIBNAME1 );
		yl_PrintString( (int32)TMP572, (ylSTRING*)vr44, 1 );
		yl_StrDelete( (ylSTRING*)&TMP712 );
	}
	goto label49;
	label50:;
	yl_FileClose( (int32)F11 );
	yl_FileClose( (int32)F21 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	goto label43;
	label44:;
	int32 vr50 = yl_ErrorGetNum(  );
	ylSTRING* vr54 = yl_LongintToStr( ((int64)vr50 * 3ll) + 1ll );
	__builtin_memset( &TMP721, 0, 24ll );
	ylSTRING* vr57 = yl_StrConcat( &TMP721, (void*)"error:", 7ll, (void*)vr54, -1ll );
	yl_PrintString( 0, (ylSTRING*)vr57, 1 );
	int32 vr58 = yl_ErrorGetNum(  );
	yl_End( (int32)(((int64)vr58 * 3ll) + 1ll) );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	label43:;
	yl_ErrorSetHandler( TMP511 );
}

ylSTRING* INVAR( ylSTRING* A1 )
{
	ylSTRING TMP751;
	ylSTRING TMP761;
	ylSTRING TMP771;
	ylSTRING ylresult1;
	__builtin_memset( &ylresult1, 0, 24ll );
	label53:;
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	int64 F1;
	__builtin_memset( &F1, 0, 8ll );
	int32 vr3 = yl_FileFree(  );
	F1 = (int64)vr3;
	ylSTRING* vr6 = yl_ExePath(  );
	__builtin_memset( &TMP751, 0, 24ll );
	ylSTRING* vr9 = yl_StrConcat( &TMP751, (void*)vr6, -1ll, (void*)"/conf/", 7ll );
	__builtin_memset( &TMP761, 0, 24ll );
	ylSTRING* vr12 = yl_StrConcat( &TMP761, (void*)vr9, -1ll, (void*)A1, -1ll );
	__builtin_memset( &TMP771, 0, 24ll );
	ylSTRING* vr15 = yl_StrConcat( &TMP771, (void*)vr12, -1ll, (void*)".conf", 6ll );
	yl_FileOpenEncod( (ylSTRING*)vr15, 2u, 0u, 0u, (int32)F1, 0, (uint8*)"ascii" );
	yl_FileLineInput( (int32)F1, (void*)&TMP1, -1ll, 0 );
	yl_FileClose( (int32)F1 );
	yl_StrInit( (void*)&ylresult1, -1ll, (void*)&TMP1, -1ll, 0 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	goto label54;
	yl_StrDelete( (ylSTRING*)&TMP1 );
	label54:;
	ylSTRING* vr24 = yl_StrAllocTempResult( (ylSTRING*)&ylresult1 );
	return vr24;
}

void MAKEFILE( int64 A1, int64 B1, ylSTRING* C1, ylSTRING* D1 )
{
	label55:;
	int64 F11;
	__builtin_memset( &F11, 0, 8ll );
	int64 F21;
	__builtin_memset( &F21, 0, 8ll );
	int64 F31;
	__builtin_memset( &F31, 0, 8ll );
	int64 OF1;
	__builtin_memset( &OF1, 0, 8ll );
	ylSTRING TMP1;
	__builtin_memset( &TMP1, 0, 24ll );
	ylSTRING TMP21;
	__builtin_memset( &TMP21, 0, 24ll );
	ylSTRING SIGN1;
	__builtin_memset( &SIGN1, 0, 24ll );
	ylSTRING COMNAME1;
	__builtin_memset( &COMNAME1, 0, 24ll );
	int64 I1;
	__builtin_memset( &I1, 0, 8ll );
	ylSTRING COUNTSIGN1;
	__builtin_memset( &COUNTSIGN1, 0, 24ll );
	ylSTRING VALUE1;
	__builtin_memset( &VALUE1, 0, 24ll );
	ylSTRING YURONC1;
	__builtin_memset( &YURONC1, 0, 24ll );
	ylSTRING Y21;
	__builtin_memset( &Y21, 0, 24ll );
	ylSTRING MATHSIGN1;
	__builtin_memset( &MATHSIGN1, 0, 24ll );
	ylSTRING MATHNUMSIGN1;
	__builtin_memset( &MATHNUMSIGN1, 0, 24ll );
	ylSTRING MATHTMP1;
	__builtin_memset( &MATHTMP1, 0, 24ll );
	ylSTRING CLASSFNAME1;
	__builtin_memset( &CLASSFNAME1, 0, 24ll );
	ylSTRING CLASSSNAME1;
	__builtin_memset( &CLASSSNAME1, 0, 24ll );
	ylSTRING LIBNAME1;
	__builtin_memset( &LIBNAME1, 0, 24ll );
	ylSTRING OBNAME1;
	__builtin_memset( &OBNAME1, 0, 24ll );
	ylSTRING OBJNAME1;
	__builtin_memset( &OBJNAME1, 0, 24ll );
	ylSTRING OBCASE1;
	__builtin_memset( &OBCASE1, 0, 24ll );
	int64 MODELTMP1;
	__builtin_memset( &MODELTMP1, 0, 8ll );
	yl_StrAssign( (void*)&YURONC1, -1ll, (void*)C1, -1ll, 0 );
	F11 = A1;
	F21 = B1;
	label57:;
	int32 vr25 = yl_FileEof( (int32)F11 );
	if( (int64)vr25 == -1ll ) goto label58;
	{
		yl_FileLineInput( (int32)F11, (void*)&TMP1, -1ll, 0 );
		LINEN = LINEN + 1ll;
		I1 = 0ll;
		label59:;
		int64 vr31 = yl_StrLen( (void*)&TMP1, -1ll );
		if( I1 > vr31 ) goto label60;
		{
			int64 vr33 = yl_StrLen( (void*)&TMP1, -1ll );
			if( vr33 != 0ll ) goto label62;
			{
				goto label60;
				label62:;
			}
			I1 = I1 + 1ll;
			ylSTRING* vr36 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
			yl_StrAssign( (void*)&SIGN1, -1ll, (void*)vr36, -1ll, 0 );
			int32 vr39 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"/", 2ll );
			ylSTRING* vr44 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
			int32 vr45 = yl_StrCompare( (void*)vr44, -1ll, (void*)"*", 2ll );
			if( ((int64)-((int64)vr39 == 0ll) & (int64)-((int64)vr45 == 0ll)) == 0ll ) goto label64;
			{
				label65:;
				int64 vr50 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr50 ) goto label66;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr53 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr54 = yl_StrCompare( (void*)vr53, -1ll, (void*)"/", 2ll );
					ylSTRING* vr59 = yl_StrMid( (ylSTRING*)&TMP1, I1 + -1ll, 1ll );
					int32 vr60 = yl_StrCompare( (void*)vr59, -1ll, (void*)"*", 2ll );
					if( ((int64)-((int64)vr54 == 0ll) & (int64)-((int64)vr60 == 0ll)) == 0ll ) goto label68;
					{
						goto label66;
						label68:;
					}
				}
				goto label65;
				label66:;
			}
			label64:;
			label63:;
			int32 vr65 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"/", 2ll );
			ylSTRING* vr70 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
			int32 vr71 = yl_StrCompare( (void*)vr70, -1ll, (void*)"/", 2ll );
			if( ((int64)-((int64)vr65 == 0ll) & (int64)-((int64)vr71 == 0ll)) == 0ll ) goto label70;
			{
				goto label60;
				label70:;
			}
			int32 vr76 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"#", 2ll );
			if( (int64)vr76 != 0ll ) goto label72;
			{
				ylSTRING TMP814;
				ylSTRING TMP824;
				ylSTRING TMP834;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label73:;
				int64 vr80 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr80 ) goto label74;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr83 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr84 = yl_StrCompare( (void*)vr83, -1ll, (void*)"(", 2ll );
					if( (int64)vr84 != 0ll ) goto label76;
					{
						goto label74;
						label76:;
					}
					ylSTRING* vr87 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr87, -1ll, 0 );
				}
				goto label73;
				label74:;
				__builtin_memset( &TMP834, 0, 24ll );
				__builtin_memset( &TMP824, 0, 24ll );
				yl_StrAssign( (void*)&TMP824, -1ll, (void*)"", 1ll, 0 );
				__builtin_memset( &TMP814, 0, 24ll );
				yl_StrAssign( (void*)&TMP814, -1ll, (void*)" ", 2ll, 0 );
				ylSTRING* vr97 = REPLACE( &TMP21, &TMP814, &TMP824 );
				yl_StrAssign( (void*)&TMP834, -1ll, (void*)vr97, -1ll, 0 );
				ylSTRING* vr100 = AHR( &TMP834 );
				yl_StrAssign( (void*)&OBJNAME1, -1ll, (void*)vr100, -1ll, 0 );
				yl_StrDelete( (ylSTRING*)&TMP834 );
				yl_StrDelete( (ylSTRING*)&TMP824 );
				yl_StrDelete( (ylSTRING*)&TMP814 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				int32 vr106 = yl_FileFree(  );
				OF1 = (int64)vr106;
				yl_FileOpenEncod( (ylSTRING*)&OBJECTLIB, 2u, 0u, 0u, (int32)OF1, 0, (uint8*)"ascii" );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label77:;
				int32 vr111 = yl_FileEof( (int32)OF1 );
				if( (int64)vr111 == -1ll ) goto label78;
				{
					ylSTRING TMP845;
					ylSTRING TMP855;
					yl_FileLineInput( (int32)OF1, (void*)&TMP21, -1ll, 0 );
					__builtin_memset( &TMP845, 0, 24ll );
					ylSTRING* vr118 = yl_StrConcat( &TMP845, (void*)"<", 2ll, (void*)&OBJNAME1, -1ll );
					__builtin_memset( &TMP855, 0, 24ll );
					ylSTRING* vr121 = yl_StrConcat( &TMP855, (void*)vr118, -1ll, (void*)">", 2ll );
					int32 vr123 = yl_StrCompare( (void*)&TMP21, -1ll, (void*)vr121, -1ll );
					if( (int64)vr123 != 0ll ) goto label80;
					{
						goto label78;
						label80:;
					}
				}
				goto label77;
				label78:;
				ylSTRING* vr127 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
				int32 vr128 = yl_StrCompare( (void*)vr127, -1ll, (void*)")", 2ll );
				if( (int64)vr128 != 0ll ) goto label82;
				{
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label83:;
					int32 vr132 = yl_FileEof( (int32)OF1 );
					if( (int64)vr132 == -1ll ) goto label84;
					{
						ylSTRING TMP876;
						ylSTRING TMP886;
						int64 TMP896;
						yl_FileLineInput( (int32)OF1, (void*)&TMP21, -1ll, 0 );
						__builtin_memset( &TMP876, 0, 24ll );
						ylSTRING* vr139 = yl_StrConcat( &TMP876, (void*)"</", 3ll, (void*)&OBJNAME1, -1ll );
						__builtin_memset( &TMP886, 0, 24ll );
						ylSTRING* vr142 = yl_StrConcat( &TMP886, (void*)vr139, -1ll, (void*)">", 2ll );
						int32 vr144 = yl_StrCompare( (void*)&TMP21, -1ll, (void*)vr142, -1ll );
						if( (int64)vr144 != 0ll ) goto label86;
						{
							goto label84;
							label86:;
						}
						TMP896 = F21;
						yl_PrintString( (int32)TMP896, (ylSTRING*)&TMP21, 1 );
					}
					goto label83;
					label84:;
				}
				goto label81;
				label82:;
				{
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label87:;
					int64 vr150 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr150 ) goto label88;
					{
						int64 TMP1096;
						int64 TMP1116;
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
						label89:;
						int64 vr153 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr153 ) goto label90;
						{
							yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
							I1 = I1 + 1ll;
							ylSTRING* vr157 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr158 = yl_StrCompare( (void*)vr157, -1ll, (void*)",", 2ll );
							if( (int64)vr158 != 0ll ) goto label92;
							{
								goto label90;
								label92:;
							}
							ylSTRING* vr161 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr162 = yl_StrCompare( (void*)vr161, -1ll, (void*)")", 2ll );
							if( (int64)vr162 != 0ll ) goto label94;
							{
								MODELTMP1 = 1ll;
								goto label90;
							}
							label94:;
							label93:;
							ylSTRING* vr165 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr166 = yl_StrCompare( (void*)vr165, -1ll, (void*)"+", 2ll );
							if( (int64)vr166 != 0ll ) goto label96;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"+", 2ll, 0 );
							}
							label96:;
							label95:;
							ylSTRING* vr170 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr171 = yl_StrCompare( (void*)vr170, -1ll, (void*)"&", 2ll );
							if( (int64)vr171 != 0ll ) goto label98;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"&", 2ll, 0 );
							}
							label98:;
							label97:;
							ylSTRING* vr175 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr176 = yl_StrCompare( (void*)vr175, -1ll, (void*)"-", 2ll );
							if( (int64)vr176 != 0ll ) goto label100;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"-", 2ll, 0 );
							}
							label100:;
							label99:;
							ylSTRING* vr180 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr181 = yl_StrCompare( (void*)vr180, -1ll, (void*)"*", 2ll );
							if( (int64)vr181 != 0ll ) goto label102;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"*", 2ll, 0 );
							}
							label102:;
							label101:;
							ylSTRING* vr185 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr186 = yl_StrCompare( (void*)vr185, -1ll, (void*)"/", 2ll );
							if( (int64)vr186 != 0ll ) goto label104;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"/", 2ll, 0 );
							}
							label104:;
							label103:;
							ylSTRING* vr190 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr191 = yl_StrCompare( (void*)vr190, -1ll, (void*)"\x5C", 2ll );
							if( (int64)vr191 != 0ll ) goto label106;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"\x5C", 2ll, 0 );
							}
							label106:;
							label105:;
							ylSTRING* vr195 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr196 = yl_StrCompare( (void*)vr195, -1ll, (void*)"|", 2ll );
							if( (int64)vr196 != 0ll ) goto label108;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"|", 2ll, 0 );
							}
							label108:;
							label107:;
							ylSTRING* vr200 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr201 = yl_StrCompare( (void*)vr200, -1ll, (void*)"=", 2ll );
							if( (int64)vr201 != 0ll ) goto label110;
							{
								yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
							}
							label110:;
							label109:;
							ylSTRING* vr205 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr206 = yl_StrCompare( (void*)vr205, -1ll, (void*)"<", 2ll );
							if( (int64)vr206 != 0ll ) goto label112;
							{
								int64 TMP978;
								int64 TMP998;
								int64 TMP1008;
								int64 TMP1028;
								yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
								label113:;
								int64 vr210 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr210 ) goto label114;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr213 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr214 = yl_StrCompare( (void*)vr213, -1ll, (void*)">", 2ll );
									if( (int64)vr214 != 0ll ) goto label116;
									{
										goto label114;
										label116:;
									}
									ylSTRING* vr217 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr217, -1ll, 0 );
								}
								goto label113;
								label114:;
								TMP978 = F21;
								ylSTRING* vr219 = yl_StrAllocTempDescZEx( (uint8*)"add", 3ll );
								yl_PrintString( (int32)TMP978, (ylSTRING*)vr219, 1 );
								TMP998 = F21;
								ylSTRING* vr222 = AHR( &TMP21 );
								yl_PrintString( (int32)TMP998, (ylSTRING*)vr222, 1 );
								TMP1008 = F21;
								ylSTRING* vr224 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
								yl_PrintString( (int32)TMP1008, (ylSTRING*)vr224, 1 );
								TMP1028 = F21;
								yl_PrintString( (int32)TMP1028, (ylSTRING*)&MATHSIGN1, 1 );
								yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
							}
							label112:;
							label111:;
							ylSTRING* vr230 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr231 = yl_StrCompare( (void*)vr230, -1ll, (void*)"\x22", 2ll );
							if( (int64)vr231 != 0ll ) goto label118;
							{
								int64 TMP1048;
								int64 TMP1068;
								int64 TMP1078;
								int64 TMP1088;
								yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
								label119:;
								int64 vr235 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr235 ) goto label120;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr238 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr239 = yl_StrCompare( (void*)vr238, -1ll, (void*)"\x22", 2ll );
									if( (int64)vr239 != 0ll ) goto label122;
									{
										goto label120;
										label122:;
									}
									ylSTRING* vr242 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr242, -1ll, 0 );
								}
								goto label119;
								label120:;
								TMP1048 = F21;
								ylSTRING* vr244 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
								yl_PrintString( (int32)TMP1048, (ylSTRING*)vr244, 1 );
								TMP1068 = F21;
								yl_PrintString( (int32)TMP1068, (ylSTRING*)&TMP21, 1 );
								TMP1078 = F21;
								ylSTRING* vr248 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
								yl_PrintString( (int32)TMP1078, (ylSTRING*)vr248, 1 );
								TMP1088 = F21;
								yl_PrintString( (int32)TMP1088, (ylSTRING*)&MATHSIGN1, 1 );
								yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
							}
							label118:;
							label117:;
						}
						goto label89;
						label90:;
						yl_FileLineInput( (int32)OF1, (void*)&TMP21, -1ll, 0 );
						TMP1096 = F21;
						ylSTRING* vr255 = yl_StrAllocTempDescZEx( (uint8*)"mov", 3ll );
						yl_PrintString( (int32)TMP1096, (ylSTRING*)vr255, 1 );
						TMP1116 = F21;
						yl_PrintString( (int32)TMP1116, (ylSTRING*)&TMP21, 1 );
						if( MODELTMP1 != 1ll ) goto label124;
						{
							MODELTMP1 = 0ll;
							goto label88;
						}
						label124:;
						label123:;
					}
					goto label87;
					label88:;
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label125:;
					int32 vr261 = yl_FileEof( (int32)OF1 );
					if( (int64)vr261 == -1ll ) goto label126;
					{
						ylSTRING TMP1126;
						ylSTRING TMP1136;
						int64 TMP1146;
						yl_FileLineInput( (int32)OF1, (void*)&TMP21, -1ll, 0 );
						__builtin_memset( &TMP1126, 0, 24ll );
						ylSTRING* vr268 = yl_StrConcat( &TMP1126, (void*)"</", 3ll, (void*)&OBJNAME1, -1ll );
						__builtin_memset( &TMP1136, 0, 24ll );
						ylSTRING* vr271 = yl_StrConcat( &TMP1136, (void*)vr268, -1ll, (void*)">", 2ll );
						int32 vr273 = yl_StrCompare( (void*)&TMP21, -1ll, (void*)vr271, -1ll );
						if( (int64)vr273 != 0ll ) goto label128;
						{
							goto label126;
							label128:;
						}
						TMP1146 = F21;
						yl_PrintString( (int32)TMP1146, (ylSTRING*)&TMP21, 1 );
					}
					goto label125;
					label126:;
				}
				label81:;
			}
			label72:;
			label71:;
			int32 vr278 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"!", 2ll );
			if( (int64)vr278 != 0ll ) goto label130;
			{
				ylSTRING TMP1164;
				ylSTRING TMP1174;
				ylSTRING TMP1184;
				ylSTRING TMP1204;
				ylSTRING TMP1214;
				ylSTRING TMP1224;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label131:;
				int64 vr282 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr282 ) goto label132;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr285 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr286 = yl_StrCompare( (void*)vr285, -1ll, (void*)" ", 2ll );
					if( (int64)vr286 != 0ll ) goto label134;
					{
						goto label132;
						label134:;
					}
					ylSTRING* vr289 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr289, -1ll, 0 );
				}
				goto label131;
				label132:;
				__builtin_memset( &TMP1184, 0, 24ll );
				__builtin_memset( &TMP1174, 0, 24ll );
				yl_StrAssign( (void*)&TMP1174, -1ll, (void*)"", 1ll, 0 );
				__builtin_memset( &TMP1164, 0, 24ll );
				yl_StrAssign( (void*)&TMP1164, -1ll, (void*)" ", 2ll, 0 );
				ylSTRING* vr299 = REPLACE( &TMP21, &TMP1164, &TMP1174 );
				yl_StrAssign( (void*)&TMP1184, -1ll, (void*)vr299, -1ll, 0 );
				ylSTRING* vr302 = AHR( &TMP1184 );
				yl_StrAssign( (void*)&OBNAME1, -1ll, (void*)vr302, -1ll, 0 );
				yl_StrDelete( (ylSTRING*)&TMP1184 );
				yl_StrDelete( (ylSTRING*)&TMP1174 );
				yl_StrDelete( (ylSTRING*)&TMP1164 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label135:;
				int64 vr309 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr309 ) goto label136;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr312 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr313 = yl_StrCompare( (void*)vr312, -1ll, (void*)"=", 2ll );
					if( (int64)vr313 != 0ll ) goto label138;
					{
						goto label136;
						label138:;
					}
				}
				goto label135;
				label136:;
				label139:;
				int64 vr316 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr316 ) goto label140;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr319 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr320 = yl_StrCompare( (void*)vr319, -1ll, (void*)";", 2ll );
					if( (int64)vr320 != 0ll ) goto label142;
					{
						goto label140;
						label142:;
					}
					ylSTRING* vr323 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr323, -1ll, 0 );
				}
				goto label139;
				label140:;
				__builtin_memset( &TMP1224, 0, 24ll );
				__builtin_memset( &TMP1214, 0, 24ll );
				yl_StrAssign( (void*)&TMP1214, -1ll, (void*)"", 1ll, 0 );
				__builtin_memset( &TMP1204, 0, 24ll );
				yl_StrAssign( (void*)&TMP1204, -1ll, (void*)" ", 2ll, 0 );
				ylSTRING* vr333 = REPLACE( &TMP21, &TMP1204, &TMP1214 );
				yl_StrAssign( (void*)&TMP1224, -1ll, (void*)vr333, -1ll, 0 );
				ylSTRING* vr336 = AHR( &TMP1224 );
				yl_StrAssign( (void*)&LIBNAME1, -1ll, (void*)vr336, -1ll, 0 );
				yl_StrDelete( (ylSTRING*)&TMP1224 );
				yl_StrDelete( (ylSTRING*)&TMP1214 );
				yl_StrDelete( (ylSTRING*)&TMP1204 );
				LOADLIB( &LIBNAME1, &OBNAME1 );
			}
			label130:;
			label129:;
			int32 vr344 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"<", 2ll );
			if( (int64)vr344 != 0ll ) goto label144;
			{
				int64 TMP1314;
				int64 TMP1324;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label145:;
				int64 vr348 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr348 ) goto label146;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr351 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr352 = yl_StrCompare( (void*)vr351, -1ll, (void*)">", 2ll );
					if( (int64)vr352 != 0ll ) goto label148;
					{
						goto label146;
						label148:;
					}
					ylSTRING* vr355 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr355, -1ll, 0 );
				}
				goto label145;
				label146:;
				ylSTRING* vr358 = AHR( &TMP21 );
				yl_StrAssign( (void*)&MATHNUMSIGN1, -1ll, (void*)vr358, -1ll, 0 );
				yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
				label149:;
				int64 vr362 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr362 ) goto label150;
				{
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					I1 = I1 + 1ll;
					ylSTRING* vr366 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr367 = yl_StrCompare( (void*)vr366, -1ll, (void*)";", 2ll );
					if( (int64)vr367 != 0ll ) goto label152;
					{
						goto label150;
						label152:;
					}
					ylSTRING* vr370 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr371 = yl_StrCompare( (void*)vr370, -1ll, (void*)"+", 2ll );
					if( (int64)vr371 != 0ll ) goto label154;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"+", 2ll, 0 );
					}
					label154:;
					label153:;
					ylSTRING* vr375 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr376 = yl_StrCompare( (void*)vr375, -1ll, (void*)"&", 2ll );
					if( (int64)vr376 != 0ll ) goto label156;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"&", 2ll, 0 );
					}
					label156:;
					label155:;
					ylSTRING* vr380 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr381 = yl_StrCompare( (void*)vr380, -1ll, (void*)"-", 2ll );
					if( (int64)vr381 != 0ll ) goto label158;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"-", 2ll, 0 );
					}
					label158:;
					label157:;
					ylSTRING* vr385 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr386 = yl_StrCompare( (void*)vr385, -1ll, (void*)"*", 2ll );
					if( (int64)vr386 != 0ll ) goto label160;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"*", 2ll, 0 );
					}
					label160:;
					label159:;
					ylSTRING* vr390 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr391 = yl_StrCompare( (void*)vr390, -1ll, (void*)"/", 2ll );
					if( (int64)vr391 != 0ll ) goto label162;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"/", 2ll, 0 );
					}
					label162:;
					label161:;
					ylSTRING* vr395 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr396 = yl_StrCompare( (void*)vr395, -1ll, (void*)"\x5C", 2ll );
					if( (int64)vr396 != 0ll ) goto label164;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"\x5C", 2ll, 0 );
					}
					label164:;
					label163:;
					ylSTRING* vr400 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr401 = yl_StrCompare( (void*)vr400, -1ll, (void*)"|", 2ll );
					if( (int64)vr401 != 0ll ) goto label166;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"|", 2ll, 0 );
					}
					label166:;
					label165:;
					ylSTRING* vr405 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr406 = yl_StrCompare( (void*)vr405, -1ll, (void*)"=", 2ll );
					if( (int64)vr406 != 0ll ) goto label168;
					{
						yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
					}
					label168:;
					label167:;
					ylSTRING* vr410 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr411 = yl_StrCompare( (void*)vr410, -1ll, (void*)"<", 2ll );
					if( (int64)vr411 != 0ll ) goto label170;
					{
						int64 TMP1236;
						int64 TMP1246;
						int64 TMP1256;
						int64 TMP1266;
						yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
						label171:;
						int64 vr415 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr415 ) goto label172;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr418 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr419 = yl_StrCompare( (void*)vr418, -1ll, (void*)">", 2ll );
							if( (int64)vr419 != 0ll ) goto label174;
							{
								goto label172;
								label174:;
							}
							ylSTRING* vr422 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr422, -1ll, 0 );
						}
						goto label171;
						label172:;
						TMP1236 = F21;
						ylSTRING* vr424 = yl_StrAllocTempDescZEx( (uint8*)"add", 3ll );
						yl_PrintString( (int32)TMP1236, (ylSTRING*)vr424, 1 );
						TMP1246 = F21;
						ylSTRING* vr427 = AHR( &TMP21 );
						yl_PrintString( (int32)TMP1246, (ylSTRING*)vr427, 1 );
						TMP1256 = F21;
						ylSTRING* vr429 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
						yl_PrintString( (int32)TMP1256, (ylSTRING*)vr429, 1 );
						TMP1266 = F21;
						yl_PrintString( (int32)TMP1266, (ylSTRING*)&MATHSIGN1, 1 );
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					}
					label170:;
					label169:;
					ylSTRING* vr435 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr436 = yl_StrCompare( (void*)vr435, -1ll, (void*)"\x22", 2ll );
					if( (int64)vr436 != 0ll ) goto label176;
					{
						int64 TMP1276;
						int64 TMP1286;
						int64 TMP1296;
						int64 TMP1306;
						yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
						label177:;
						int64 vr440 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr440 ) goto label178;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr443 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr444 = yl_StrCompare( (void*)vr443, -1ll, (void*)"\x22", 2ll );
							if( (int64)vr444 != 0ll ) goto label180;
							{
								goto label178;
								label180:;
							}
							ylSTRING* vr447 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr447, -1ll, 0 );
						}
						goto label177;
						label178:;
						TMP1276 = F21;
						ylSTRING* vr449 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
						yl_PrintString( (int32)TMP1276, (ylSTRING*)vr449, 1 );
						TMP1286 = F21;
						yl_PrintString( (int32)TMP1286, (ylSTRING*)&TMP21, 1 );
						TMP1296 = F21;
						ylSTRING* vr453 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
						yl_PrintString( (int32)TMP1296, (ylSTRING*)vr453, 1 );
						TMP1306 = F21;
						yl_PrintString( (int32)TMP1306, (ylSTRING*)&MATHSIGN1, 1 );
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					}
					label176:;
					label175:;
				}
				goto label149;
				label150:;
				TMP1314 = F21;
				ylSTRING* vr458 = yl_StrAllocTempDescZEx( (uint8*)"mov", 3ll );
				yl_PrintString( (int32)TMP1314, (ylSTRING*)vr458, 1 );
				TMP1324 = F21;
				yl_PrintString( (int32)TMP1324, (ylSTRING*)&MATHNUMSIGN1, 1 );
			}
			label144:;
			label143:;
			int32 vr463 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"}", 2ll );
			if( (int64)vr463 != 0ll ) goto label182;
			{
				int64 TMP1344;
				int64 TMP1354;
				ylSTRING TMP1364;
				ylSTRING TMP1374;
				CNUM = CNUM + 1ll;
				TMP1344 = F21;
				yl_PrintString( (int32)TMP1344, (ylSTRING*)D1, 1 );
				TMP1354 = F21;
				__builtin_memset( &TMP1364, 0, 24ll );
				ylSTRING* vr470 = yl_StrConcat( &TMP1364, (void*)"</", 3ll, (void*)&YURONC1, -1ll );
				__builtin_memset( &TMP1374, 0, 24ll );
				ylSTRING* vr473 = yl_StrConcat( &TMP1374, (void*)vr470, -1ll, (void*)">", 2ll );
				yl_PrintString( (int32)TMP1354, (ylSTRING*)vr473, 1 );
				yl_StrDelete( (ylSTRING*)&OBCASE1 );
				yl_StrDelete( (ylSTRING*)&OBJNAME1 );
				yl_StrDelete( (ylSTRING*)&OBNAME1 );
				yl_StrDelete( (ylSTRING*)&LIBNAME1 );
				yl_StrDelete( (ylSTRING*)&CLASSSNAME1 );
				yl_StrDelete( (ylSTRING*)&CLASSFNAME1 );
				yl_StrDelete( (ylSTRING*)&MATHTMP1 );
				yl_StrDelete( (ylSTRING*)&MATHNUMSIGN1 );
				yl_StrDelete( (ylSTRING*)&MATHSIGN1 );
				yl_StrDelete( (ylSTRING*)&Y21 );
				yl_StrDelete( (ylSTRING*)&YURONC1 );
				yl_StrDelete( (ylSTRING*)&VALUE1 );
				yl_StrDelete( (ylSTRING*)&COUNTSIGN1 );
				yl_StrDelete( (ylSTRING*)&COMNAME1 );
				yl_StrDelete( (ylSTRING*)&SIGN1 );
				yl_StrDelete( (ylSTRING*)&TMP21 );
				yl_StrDelete( (ylSTRING*)&TMP1 );
				goto label56;
			}
			label182:;
			label181:;
			int32 vr493 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"[", 2ll );
			if( (int64)vr493 != 0ll ) goto label184;
			{
				int64 TMP1384;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label185:;
				int64 vr497 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr497 ) goto label186;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr500 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr501 = yl_StrCompare( (void*)vr500, -1ll, (void*)"]", 2ll );
					ylSTRING* vr506 = yl_StrMid( (ylSTRING*)&TMP1, I1 + -1ll, 1ll );
					int32 vr507 = yl_StrCompare( (void*)vr506, -1ll, (void*)"/", 2ll );
					if( ((int64)-((int64)vr501 == 0ll) & (int64)-((int64)vr507 == 0ll)) == 0ll ) goto label188;
					{
						goto label186;
						label188:;
					}
					ylSTRING* vr512 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr512, -1ll, 0 );
				}
				goto label185;
				label186:;
				TMP1384 = F21;
				int64 vr515 = yl_StrLen( (void*)&TMP21, -1ll );
				ylSTRING* vr518 = yl_StrMid( (ylSTRING*)&TMP21, 1ll, vr515 + -1ll );
				yl_PrintString( (int32)TMP1384, (ylSTRING*)vr518, 1 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
			}
			label184:;
			label183:;
			int32 vr522 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"", 2ll );
			ylSTRING* vr527 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
			int32 vr528 = yl_StrCompare( (void*)vr527, -1ll, (void*)"[", 2ll );
			if( ((int64)-((int64)vr522 == 0ll) & (int64)-((int64)vr528 == 0ll)) == 0ll ) goto label190;
			{
				int64 TMP1404;
				ylSTRING TMP1414;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				I1 = I1 + 1ll;
				label191:;
				int64 vr535 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr535 ) goto label192;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr538 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr539 = yl_StrCompare( (void*)vr538, -1ll, (void*)"]", 2ll );
					ylSTRING* vr544 = yl_StrMid( (ylSTRING*)&TMP1, I1 + -1ll, 1ll );
					int32 vr545 = yl_StrCompare( (void*)vr544, -1ll, (void*)"/", 2ll );
					if( ((int64)-((int64)vr539 == 0ll) & (int64)-((int64)vr545 == 0ll)) == 0ll ) goto label194;
					{
						goto label192;
						label194:;
					}
					ylSTRING* vr550 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr550, -1ll, 0 );
				}
				goto label191;
				label192:;
				TMP1404 = F21;
				__builtin_memset( &TMP1414, 0, 24ll );
				int64 vr554 = yl_StrLen( (void*)&TMP21, -1ll );
				ylSTRING* vr557 = yl_StrMid( (ylSTRING*)&TMP21, 1ll, vr554 + -1ll );
				yl_StrAssign( (void*)&TMP1414, -1ll, (void*)vr557, -1ll, 0 );
				ylSTRING* vr560 = AHR( &TMP1414 );
				yl_PrintString( (int32)TMP1404, (ylSTRING*)vr560, 1 );
				yl_StrDelete( (ylSTRING*)&TMP1414 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
			}
			label190:;
			label189:;
			int32 vr565 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"@", 2ll );
			if( (int64)vr565 != 0ll ) goto label196;
			{
				ylSTRING TMP1434;
				int64 TMP1444;
				int64 TMP1454;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label197:;
				int64 vr569 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr569 ) goto label198;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr572 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr573 = yl_StrCompare( (void*)vr572, -1ll, (void*)";", 2ll );
					if( (int64)vr573 != 0ll ) goto label200;
					{
						goto label198;
						label200:;
					}
					ylSTRING* vr576 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr576, -1ll, 0 );
				}
				goto label197;
				label198:;
				int32 vr578 = yl_FileFree(  );
				F31 = (int64)vr578;
				__builtin_memset( &TMP1434, 0, 24ll );
				ylSTRING* vr583 = yl_StrConcat( &TMP1434, (void*)&OUTFILE, -1ll, (void*)".conf", 6ll );
				yl_FileOpenEncod( (ylSTRING*)vr583, 3u, 0u, 0u, (int32)F31, 0, (uint8*)"ascii" );
				TMP1444 = F31;
				ylSTRING* vr585 = AHR( &TMP21 );
				yl_PrintString( (int32)TMP1444, (ylSTRING*)vr585, 1 );
				TMP1454 = F31;
				ylSTRING* vr587 = yl_StrAllocTempDescZEx( (uint8*)"this is the loading point", 25ll );
				yl_PrintString( (int32)TMP1454, (ylSTRING*)vr587, 1 );
				yl_FileClose( (int32)F31 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
			}
			label196:;
			label195:;
			int32 vr592 = yl_StrCompare( (void*)&SIGN1, -1ll, (void*)"+", 2ll );
			if( (int64)vr592 != 0ll ) goto label202;
			{
				ylSTRING TMP1484;
				int64 TMP1494;
				ylSTRING TMP1514;
				int64 TMP1524;
				ylSTRING TMP1684;
				int64 TMP1694;
				ylSTRING TMP2004;
				int64 TMP2014;
				ylSTRING TMP2254;
				int64 TMP2264;
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				label203:;
				int64 vr596 = yl_StrLen( (void*)&TMP1, -1ll );
				if( I1 > vr596 ) goto label204;
				{
					I1 = I1 + 1ll;
					ylSTRING* vr599 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					int32 vr600 = yl_StrCompare( (void*)vr599, -1ll, (void*)" ", 2ll );
					if( (int64)vr600 != 0ll ) goto label206;
					{
						goto label204;
						label206:;
					}
					ylSTRING* vr603 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
					yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr603, -1ll, 0 );
				}
				goto label203;
				label204:;
				yl_StrAssign( (void*)&COMNAME1, -1ll, (void*)&TMP21, -1ll, 0 );
				yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
				__builtin_memset( &TMP1484, 0, 24ll );
				yl_StrAssign( (void*)&TMP1484, -1ll, (void*)"include", 8ll, 0 );
				ylSTRING* vr611 = INVAR( &TMP1484 );
				int32 vr613 = yl_StrCompare( (void*)&COMNAME1, -1ll, (void*)vr611, -1ll );
				TMP1494 = (int64)-((int64)vr613 == 0ll);
				yl_StrDelete( (ylSTRING*)&TMP1484 );
				if( TMP1494 == 0ll ) goto label208;
				{
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label209:;
					int64 vr619 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr619 ) goto label210;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr622 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr623 = yl_StrCompare( (void*)vr622, -1ll, (void*)";", 2ll );
						if( (int64)vr623 != 0ll ) goto label212;
						{
							goto label210;
							label212:;
						}
						ylSTRING* vr626 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr626, -1ll, 0 );
					}
					goto label209;
					label210:;
					yl_StrAssign( (void*)&VALUE1, -1ll, (void*)&TMP21, -1ll, 0 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					INCLUDELIB( &VALUE1 );
				}
				label208:;
				label207:;
				__builtin_memset( &TMP1514, 0, 24ll );
				yl_StrAssign( (void*)&TMP1514, -1ll, (void*)"class", 6ll, 0 );
				ylSTRING* vr635 = INVAR( &TMP1514 );
				int32 vr637 = yl_StrCompare( (void*)&COMNAME1, -1ll, (void*)vr635, -1ll );
				TMP1524 = (int64)-((int64)vr637 == 0ll);
				yl_StrDelete( (ylSTRING*)&TMP1514 );
				if( TMP1524 == 0ll ) goto label214;
				{
					ylSTRING TMP1535;
					ylSTRING TMP1545;
					ylSTRING TMP1555;
					ylSTRING TMP1575;
					ylSTRING TMP1585;
					ylSTRING TMP1595;
					ylSTRING TMP1605;
					ylSTRING TMP1615;
					ylSTRING TMP1625;
					int64 TMP1635;
					ylSTRING TMP1645;
					ylSTRING TMP1655;
					ylSTRING TMP1665;
					ONUM = ONUM + 1ll;
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label215:;
					int64 vr644 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr644 ) goto label216;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr647 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr648 = yl_StrCompare( (void*)vr647, -1ll, (void*)",", 2ll );
						if( (int64)vr648 != 0ll ) goto label218;
						{
							goto label216;
							label218:;
						}
						ylSTRING* vr651 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr651, -1ll, 0 );
					}
					goto label215;
					label216:;
					__builtin_memset( &TMP1595, 0, 24ll );
					__builtin_memset( &TMP1585, 0, 24ll );
					yl_StrAssign( (void*)&TMP1585, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP1575, 0, 24ll );
					yl_StrAssign( (void*)&TMP1575, -1ll, (void*)"{", 2ll, 0 );
					__builtin_memset( &TMP1555, 0, 24ll );
					__builtin_memset( &TMP1545, 0, 24ll );
					yl_StrAssign( (void*)&TMP1545, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP1535, 0, 24ll );
					yl_StrAssign( (void*)&TMP1535, -1ll, (void*)" ", 2ll, 0 );
					ylSTRING* vr668 = REPLACE( &TMP21, &TMP1535, &TMP1545 );
					yl_StrAssign( (void*)&TMP1555, -1ll, (void*)vr668, -1ll, 0 );
					ylSTRING* vr671 = REPLACE( &TMP1555, &TMP1575, &TMP1585 );
					yl_StrAssign( (void*)&TMP1595, -1ll, (void*)vr671, -1ll, 0 );
					ylSTRING* vr674 = AHR( &TMP1595 );
					yl_StrAssign( (void*)&CLASSSNAME1, -1ll, (void*)vr674, -1ll, 0 );
					yl_StrDelete( (ylSTRING*)&TMP1595 );
					yl_StrDelete( (ylSTRING*)&TMP1585 );
					yl_StrDelete( (ylSTRING*)&TMP1575 );
					yl_StrDelete( (ylSTRING*)&TMP1555 );
					yl_StrDelete( (ylSTRING*)&TMP1545 );
					yl_StrDelete( (ylSTRING*)&TMP1535 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label219:;
					int64 vr685 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr685 ) goto label220;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr688 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr689 = yl_StrCompare( (void*)vr688, -1ll, (void*)"{", 2ll );
						if( (int64)vr689 != 0ll ) goto label222;
						{
							goto label220;
							label222:;
						}
						ylSTRING* vr692 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr692, -1ll, 0 );
					}
					goto label219;
					label220:;
					__builtin_memset( &TMP1625, 0, 24ll );
					__builtin_memset( &TMP1615, 0, 24ll );
					yl_StrAssign( (void*)&TMP1615, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP1605, 0, 24ll );
					yl_StrAssign( (void*)&TMP1605, -1ll, (void*)" ", 2ll, 0 );
					ylSTRING* vr702 = REPLACE( &TMP21, &TMP1605, &TMP1615 );
					yl_StrAssign( (void*)&TMP1625, -1ll, (void*)vr702, -1ll, 0 );
					ylSTRING* vr705 = AHR( &TMP1625 );
					yl_StrAssign( (void*)&CLASSFNAME1, -1ll, (void*)vr705, -1ll, 0 );
					yl_StrDelete( (ylSTRING*)&TMP1625 );
					yl_StrDelete( (ylSTRING*)&TMP1615 );
					yl_StrDelete( (ylSTRING*)&TMP1605 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					TMP1635 = F21;
					__builtin_memset( &TMP1645, 0, 24ll );
					ylSTRING* vr714 = yl_StrConcat( &TMP1645, (void*)"<", 2ll, (void*)&CLASSSNAME1, -1ll );
					__builtin_memset( &TMP1655, 0, 24ll );
					ylSTRING* vr717 = yl_StrConcat( &TMP1655, (void*)vr714, -1ll, (void*)">", 2ll );
					yl_PrintString( (int32)TMP1635, (ylSTRING*)vr717, 1 );
					yl_StrAssign( (void*)&Y21, -1ll, (void*)&CLASSSNAME1, -1ll, 0 );
					__builtin_memset( &TMP1665, 0, 24ll );
					ylSTRING* vr723 = FINDCLASS( &CLASSFNAME1 );
					yl_StrAssign( (void*)&TMP1665, -1ll, (void*)vr723, -1ll, 0 );
					MAKEFILE( F11, F21, &Y21, &TMP1665 );
					yl_StrDelete( (ylSTRING*)&TMP1665 );
					int64 vr729 = yl_StrLen( (void*)&TMP1, -1ll );
					I1 = vr729;
				}
				label214:;
				label213:;
				__builtin_memset( &TMP1684, 0, 24ll );
				yl_StrAssign( (void*)&TMP1684, -1ll, (void*)"void", 5ll, 0 );
				ylSTRING* vr733 = INVAR( &TMP1684 );
				int32 vr735 = yl_StrCompare( (void*)&COMNAME1, -1ll, (void*)vr733, -1ll );
				TMP1694 = (int64)-((int64)vr735 == 0ll);
				yl_StrDelete( (ylSTRING*)&TMP1684 );
				if( TMP1694 == 0ll ) goto label224;
				{
					ylSTRING TMP1705;
					ylSTRING TMP1715;
					int64 TMP1725;
					ylSTRING TMP1735;
					ylSTRING TMP1745;
					ylSTRING TMP1985;
					ONUM = ONUM + 1ll;
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label225:;
					int64 vr742 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr742 ) goto label226;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr745 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr746 = yl_StrCompare( (void*)vr745, -1ll, (void*)"(", 2ll );
						if( (int64)vr746 != 0ll ) goto label228;
						{
							goto label226;
							label228:;
						}
						ylSTRING* vr749 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr749, -1ll, 0 );
					}
					goto label225;
					label226:;
					__builtin_memset( &TMP1715, 0, 24ll );
					yl_StrAssign( (void*)&TMP1715, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP1705, 0, 24ll );
					yl_StrAssign( (void*)&TMP1705, -1ll, (void*)" ", 2ll, 0 );
					ylSTRING* vr758 = REPLACE( &TMP21, &TMP1705, &TMP1715 );
					yl_StrAssign( (void*)&VALUE1, -1ll, (void*)vr758, -1ll, 0 );
					yl_StrDelete( (ylSTRING*)&TMP1715 );
					yl_StrDelete( (ylSTRING*)&TMP1705 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					TMP1725 = F21;
					ylSTRING* vr764 = AHR( &VALUE1 );
					__builtin_memset( &TMP1735, 0, 24ll );
					ylSTRING* vr767 = yl_StrConcat( &TMP1735, (void*)"<", 2ll, (void*)vr764, -1ll );
					__builtin_memset( &TMP1745, 0, 24ll );
					ylSTRING* vr770 = yl_StrConcat( &TMP1745, (void*)vr767, -1ll, (void*)">", 2ll );
					yl_PrintString( (int32)TMP1725, (ylSTRING*)vr770, 1 );
					ylSTRING* vr773 = AHR( &VALUE1 );
					yl_StrAssign( (void*)&Y21, -1ll, (void*)vr773, -1ll, 0 );
					ylSTRING* vr777 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
					int32 vr778 = yl_StrCompare( (void*)vr777, -1ll, (void*)")", 2ll );
					if( (int64)vr778 == 0ll ) goto label230;
					{
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
						label231:;
						int64 vr782 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr782 ) goto label232;
						{
							ylSTRING TMP1847;
							ylSTRING TMP1857;
							int64 TMP1867;
							ylSTRING TMP1957;
							ylSTRING TMP1967;
							int64 TMP1977;
							I1 = I1 + 1ll;
							ylSTRING* vr785 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr786 = yl_StrCompare( (void*)vr785, -1ll, (void*)",", 2ll );
							if( (int64)vr786 != 0ll ) goto label234;
							{
								int64 TMP1758;
								ylSTRING TMP1768;
								ylSTRING TMP1788;
								ylSTRING TMP1798;
								ylSTRING TMP1808;
								ylSTRING TMP1818;
								ylSTRING TMP1828;
								ylSTRING TMP1838;
								TMP1758 = F21;
								__builtin_memset( &TMP1838, 0, 24ll );
								__builtin_memset( &TMP1798, 0, 24ll );
								yl_StrAssign( (void*)&TMP1798, -1ll, (void*)"", 1ll, 0 );
								__builtin_memset( &TMP1788, 0, 24ll );
								yl_StrAssign( (void*)&TMP1788, -1ll, (void*)" ", 2ll, 0 );
								ylSTRING* vr796 = REPLACE( &TMP21, &TMP1788, &TMP1798 );
								__builtin_memset( &TMP1768, 0, 24ll );
								yl_StrAssign( (void*)&TMP1768, -1ll, (void*)"this", 5ll, 0 );
								ylSTRING* vr800 = INVAR( &TMP1768 );
								__builtin_memset( &TMP1808, 0, 24ll );
								ylSTRING* vr803 = yl_StrConcat( &TMP1808, (void*)"[", 2ll, (void*)vr800, -1ll );
								__builtin_memset( &TMP1818, 0, 24ll );
								ylSTRING* vr806 = yl_StrConcat( &TMP1818, (void*)vr803, -1ll, (void*)"].", 3ll );
								__builtin_memset( &TMP1828, 0, 24ll );
								ylSTRING* vr809 = yl_StrConcat( &TMP1828, (void*)vr806, -1ll, (void*)vr796, -1ll );
								yl_StrAssign( (void*)&TMP1838, -1ll, (void*)vr809, -1ll, 0 );
								ylSTRING* vr812 = AHR( &TMP1838 );
								yl_PrintString( (int32)TMP1758, (ylSTRING*)vr812, 1 );
								yl_StrDelete( (ylSTRING*)&TMP1838 );
								yl_StrDelete( (ylSTRING*)&TMP1798 );
								yl_StrDelete( (ylSTRING*)&TMP1788 );
								yl_StrDelete( (ylSTRING*)&TMP1768 );
								yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
								I1 = I1 + 1ll;
							}
							label234:;
							label233:;
							ylSTRING* vr821 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr822 = yl_StrCompare( (void*)vr821, -1ll, (void*)")", 2ll );
							__builtin_memset( &TMP1857, 0, 24ll );
							yl_StrAssign( (void*)&TMP1857, -1ll, (void*)"", 1ll, 0 );
							__builtin_memset( &TMP1847, 0, 24ll );
							yl_StrAssign( (void*)&TMP1847, -1ll, (void*)" ", 2ll, 0 );
							ylSTRING* vr832 = REPLACE( &TMP21, &TMP1847, &TMP1857 );
							int32 vr833 = yl_StrCompare( (void*)vr832, -1ll, (void*)"", 1ll );
							TMP1867 = (int64)-((int64)vr822 == 0ll) & (int64)-((int64)vr833 != 0ll);
							yl_StrDelete( (ylSTRING*)&TMP1857 );
							yl_StrDelete( (ylSTRING*)&TMP1847 );
							if( TMP1867 == 0ll ) goto label236;
							{
								int64 TMP1878;
								ylSTRING TMP1888;
								ylSTRING TMP1898;
								ylSTRING TMP1908;
								ylSTRING TMP1918;
								ylSTRING TMP1928;
								ylSTRING TMP1938;
								ylSTRING TMP1948;
								TMP1878 = F21;
								__builtin_memset( &TMP1948, 0, 24ll );
								__builtin_memset( &TMP1908, 0, 24ll );
								yl_StrAssign( (void*)&TMP1908, -1ll, (void*)"", 1ll, 0 );
								__builtin_memset( &TMP1898, 0, 24ll );
								yl_StrAssign( (void*)&TMP1898, -1ll, (void*)" ", 2ll, 0 );
								ylSTRING* vr847 = REPLACE( &TMP21, &TMP1898, &TMP1908 );
								__builtin_memset( &TMP1888, 0, 24ll );
								yl_StrAssign( (void*)&TMP1888, -1ll, (void*)"this", 5ll, 0 );
								ylSTRING* vr851 = INVAR( &TMP1888 );
								__builtin_memset( &TMP1918, 0, 24ll );
								ylSTRING* vr854 = yl_StrConcat( &TMP1918, (void*)"[", 2ll, (void*)vr851, -1ll );
								__builtin_memset( &TMP1928, 0, 24ll );
								ylSTRING* vr857 = yl_StrConcat( &TMP1928, (void*)vr854, -1ll, (void*)"].", 3ll );
								__builtin_memset( &TMP1938, 0, 24ll );
								ylSTRING* vr860 = yl_StrConcat( &TMP1938, (void*)vr857, -1ll, (void*)vr847, -1ll );
								yl_StrAssign( (void*)&TMP1948, -1ll, (void*)vr860, -1ll, 0 );
								ylSTRING* vr863 = AHR( &TMP1948 );
								yl_PrintString( (int32)TMP1878, (ylSTRING*)vr863, 1 );
								yl_StrDelete( (ylSTRING*)&TMP1948 );
								yl_StrDelete( (ylSTRING*)&TMP1908 );
								yl_StrDelete( (ylSTRING*)&TMP1898 );
								yl_StrDelete( (ylSTRING*)&TMP1888 );
								yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
								I1 = I1 + 1ll;
								goto label232;
							}
							label236:;
							label235:;
							ylSTRING* vr872 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr873 = yl_StrCompare( (void*)vr872, -1ll, (void*)")", 2ll );
							__builtin_memset( &TMP1967, 0, 24ll );
							yl_StrAssign( (void*)&TMP1967, -1ll, (void*)"", 1ll, 0 );
							__builtin_memset( &TMP1957, 0, 24ll );
							yl_StrAssign( (void*)&TMP1957, -1ll, (void*)" ", 2ll, 0 );
							ylSTRING* vr883 = REPLACE( &TMP21, &TMP1957, &TMP1967 );
							int32 vr884 = yl_StrCompare( (void*)vr883, -1ll, (void*)"", 1ll );
							TMP1977 = (int64)-((int64)vr873 == 0ll) & (int64)-((int64)vr884 == 0ll);
							yl_StrDelete( (ylSTRING*)&TMP1967 );
							yl_StrDelete( (ylSTRING*)&TMP1957 );
							if( TMP1977 == 0ll ) goto label238;
							{
								goto label232;
							}
							label238:;
							label237:;
							ylSTRING* vr891 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr891, -1ll, 0 );
						}
						goto label231;
						label232:;
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					}
					label230:;
					label229:;
					__builtin_memset( &TMP1985, 0, 24ll );
					yl_StrAssign( (void*)&TMP1985, -1ll, (void*)"", 1ll, 0 );
					MAKEFILE( F11, F21, &Y21, &TMP1985 );
					yl_StrDelete( (ylSTRING*)&TMP1985 );
				}
				label224:;
				label223:;
				__builtin_memset( &TMP2004, 0, 24ll );
				yl_StrAssign( (void*)&TMP2004, -1ll, (void*)"if", 3ll, 0 );
				ylSTRING* vr902 = INVAR( &TMP2004 );
				int32 vr904 = yl_StrCompare( (void*)&COMNAME1, -1ll, (void*)vr902, -1ll );
				TMP2014 = (int64)-((int64)vr904 == 0ll);
				yl_StrDelete( (ylSTRING*)&TMP2004 );
				if( TMP2014 == 0ll ) goto label240;
				{
					ylSTRING TMP2025;
					ylSTRING TMP2035;
					ylSTRING TMP2045;
					ONUM = ONUM + 1ll;
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label241:;
					int64 vr911 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr911 ) goto label242;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr914 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr915 = yl_StrCompare( (void*)vr914, -1ll, (void*)"(", 2ll );
						if( (int64)vr915 != 0ll ) goto label244;
						{
							goto label242;
							label244:;
						}
						ylSTRING* vr918 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr918, -1ll, 0 );
					}
					goto label241;
					label242:;
					ylSTRING IFNAME5;
					__builtin_memset( &IFNAME5, 0, 24ll );
					__builtin_memset( &TMP2045, 0, 24ll );
					__builtin_memset( &TMP2035, 0, 24ll );
					yl_StrAssign( (void*)&TMP2035, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP2025, 0, 24ll );
					yl_StrAssign( (void*)&TMP2025, -1ll, (void*)" ", 2ll, 0 );
					ylSTRING* vr929 = REPLACE( &TMP21, &TMP2025, &TMP2035 );
					yl_StrAssign( (void*)&TMP2045, -1ll, (void*)vr929, -1ll, 0 );
					ylSTRING* vr932 = AHR( &TMP2045 );
					yl_StrAssign( (void*)&IFNAME5, -1ll, (void*)vr932, -1ll, 0 );
					yl_StrDelete( (ylSTRING*)&TMP2045 );
					yl_StrDelete( (ylSTRING*)&TMP2035 );
					yl_StrDelete( (ylSTRING*)&TMP2025 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					ylSTRING* vr940 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
					int32 vr941 = yl_StrCompare( (void*)vr940, -1ll, (void*)")", 2ll );
					if( (int64)vr941 == 0ll ) goto label246;
					{
						int64 TMP2096;
						int64 TMP2106;
						int64 TMP2156;
						int64 TMP2176;
						int64 TMP2186;
						ylSTRING TMP2206;
						ylSTRING TMP2216;
						ylSTRING TMP2236;
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
						label247:;
						int64 vr945 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr945 ) goto label248;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr948 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr949 = yl_StrCompare( (void*)vr948, -1ll, (void*)")", 2ll );
							if( (int64)vr949 != 0ll ) goto label250;
							{
								goto label248;
								label250:;
							}
							ylSTRING* vr952 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr953 = yl_StrCompare( (void*)vr952, -1ll, (void*)"<", 2ll );
							if( (int64)vr953 != 0ll ) goto label252;
							{
								int64 TMP2058;
								int64 TMP2068;
								label253:;
								int64 vr956 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr956 ) goto label254;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr959 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr960 = yl_StrCompare( (void*)vr959, -1ll, (void*)">", 2ll );
									if( (int64)vr960 != 0ll ) goto label256;
									{
										goto label254;
										label256:;
									}
									ylSTRING* vr963 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr963, -1ll, 0 );
								}
								goto label253;
								label254:;
								TMP2058 = F21;
								ylSTRING* vr965 = yl_StrAllocTempDescZEx( (uint8*)"add", 3ll );
								yl_PrintString( (int32)TMP2058, (ylSTRING*)vr965, 1 );
								TMP2068 = F21;
								ylSTRING* vr968 = AHR( &TMP21 );
								yl_PrintString( (int32)TMP2068, (ylSTRING*)vr968, 1 );
								goto label248;
							}
							label252:;
							label251:;
							ylSTRING* vr971 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr972 = yl_StrCompare( (void*)vr971, -1ll, (void*)"\x22", 2ll );
							if( (int64)vr972 != 0ll ) goto label258;
							{
								int64 TMP2078;
								int64 TMP2088;
								label259:;
								int64 vr975 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr975 ) goto label260;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr978 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr979 = yl_StrCompare( (void*)vr978, -1ll, (void*)"\x22", 2ll );
									if( (int64)vr979 != 0ll ) goto label262;
									{
										goto label260;
										label262:;
									}
									ylSTRING* vr982 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr982, -1ll, 0 );
								}
								goto label259;
								label260:;
								TMP2078 = F21;
								ylSTRING* vr984 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
								yl_PrintString( (int32)TMP2078, (ylSTRING*)vr984, 1 );
								TMP2088 = F21;
								yl_PrintString( (int32)TMP2088, (ylSTRING*)&TMP21, 1 );
								goto label248;
							}
							label258:;
							label257:;
						}
						goto label247;
						label248:;
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
						label263:;
						int64 vr990 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr990 ) goto label264;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr993 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr994 = yl_StrCompare( (void*)vr993, -1ll, (void*)" ", 2ll );
							if( (int64)vr994 == 0ll ) goto label266;
							{
								goto label264;
								label266:;
							}
						}
						goto label263;
						label264:;
						I1 = I1 + -1ll;
						label267:;
						int64 vr998 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr998 ) goto label268;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr1001 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr1002 = yl_StrCompare( (void*)vr1001, -1ll, (void*)" ", 2ll );
							if( (int64)vr1002 != 0ll ) goto label270;
							{
								goto label268;
								label270:;
							}
							ylSTRING* vr1005 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1005, -1ll, 0 );
						}
						goto label267;
						label268:;
						TMP2096 = F21;
						ylSTRING* vr1007 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
						yl_PrintString( (int32)TMP2096, (ylSTRING*)vr1007, 1 );
						TMP2106 = F21;
						yl_PrintString( (int32)TMP2106, (ylSTRING*)&TMP21, 1 );
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
						label271:;
						int64 vr1013 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr1013 ) goto label272;
						{
							I1 = I1 + 1ll;
							ylSTRING* vr1016 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr1017 = yl_StrCompare( (void*)vr1016, -1ll, (void*)")", 2ll );
							if( (int64)vr1017 != 0ll ) goto label274;
							{
								goto label272;
								label274:;
							}
							ylSTRING* vr1020 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr1021 = yl_StrCompare( (void*)vr1020, -1ll, (void*)"<", 2ll );
							if( (int64)vr1021 != 0ll ) goto label276;
							{
								int64 TMP2118;
								int64 TMP2128;
								label277:;
								int64 vr1024 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr1024 ) goto label278;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr1027 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr1028 = yl_StrCompare( (void*)vr1027, -1ll, (void*)">", 2ll );
									if( (int64)vr1028 != 0ll ) goto label280;
									{
										goto label278;
										label280:;
									}
									ylSTRING* vr1031 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1031, -1ll, 0 );
								}
								goto label277;
								label278:;
								TMP2118 = F21;
								ylSTRING* vr1033 = yl_StrAllocTempDescZEx( (uint8*)"add", 3ll );
								yl_PrintString( (int32)TMP2118, (ylSTRING*)vr1033, 1 );
								TMP2128 = F21;
								ylSTRING* vr1036 = AHR( &TMP21 );
								yl_PrintString( (int32)TMP2128, (ylSTRING*)vr1036, 1 );
								goto label272;
							}
							label276:;
							label275:;
							ylSTRING* vr1039 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
							int32 vr1040 = yl_StrCompare( (void*)vr1039, -1ll, (void*)"\x22", 2ll );
							if( (int64)vr1040 != 0ll ) goto label282;
							{
								int64 TMP2138;
								int64 TMP2148;
								label283:;
								int64 vr1043 = yl_StrLen( (void*)&TMP1, -1ll );
								if( I1 > vr1043 ) goto label284;
								{
									I1 = I1 + 1ll;
									ylSTRING* vr1046 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									int32 vr1047 = yl_StrCompare( (void*)vr1046, -1ll, (void*)"\x22", 2ll );
									if( (int64)vr1047 != 0ll ) goto label286;
									{
										goto label284;
										label286:;
									}
									ylSTRING* vr1050 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
									yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1050, -1ll, 0 );
								}
								goto label283;
								label284:;
								TMP2138 = F21;
								ylSTRING* vr1052 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
								yl_PrintString( (int32)TMP2138, (ylSTRING*)vr1052, 1 );
								TMP2148 = F21;
								yl_PrintString( (int32)TMP2148, (ylSTRING*)&TMP21, 1 );
								goto label272;
							}
							label282:;
							label281:;
						}
						goto label271;
						label272:;
						yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
						TMP2156 = F21;
						ylSTRING* vr1057 = yl_StrAllocTempDescZEx( (uint8*)"logic", 5ll );
						yl_PrintString( (int32)TMP2156, (ylSTRING*)vr1057, 1 );
						TMP2176 = F21;
						yl_PrintString( (int32)TMP2176, (ylSTRING*)&IFNAME5, 1 );
						TMP2186 = F21;
						__builtin_memset( &TMP2206, 0, 24ll );
						ylSTRING* vr1064 = yl_StrConcat( &TMP2206, (void*)"<logic.", 8ll, (void*)&IFNAME5, -1ll );
						__builtin_memset( &TMP2216, 0, 24ll );
						ylSTRING* vr1067 = yl_StrConcat( &TMP2216, (void*)vr1064, -1ll, (void*)">", 2ll );
						yl_PrintString( (int32)TMP2186, (ylSTRING*)vr1067, 1 );
						yl_StrAssign( (void*)&Y21, -1ll, (void*)"logic.", 7ll, 0 );
						yl_StrConcatAssign( (void*)&Y21, -1ll, (void*)&IFNAME5, -1ll, 0 );
						__builtin_memset( &TMP2236, 0, 24ll );
						yl_StrAssign( (void*)&TMP2236, -1ll, (void*)"", 1ll, 0 );
						MAKEFILE( F11, F21, &Y21, &TMP2236 );
						yl_StrDelete( (ylSTRING*)&TMP2236 );
					}
					label246:;
					label245:;
					yl_StrDelete( (ylSTRING*)&IFNAME5 );
				}
				label240:;
				label239:;
				__builtin_memset( &TMP2254, 0, 24ll );
				yl_StrAssign( (void*)&TMP2254, -1ll, (void*)"loop", 5ll, 0 );
				ylSTRING* vr1081 = INVAR( &TMP2254 );
				int32 vr1083 = yl_StrCompare( (void*)&COMNAME1, -1ll, (void*)vr1081, -1ll );
				TMP2264 = (int64)-((int64)vr1083 == 0ll);
				yl_StrDelete( (ylSTRING*)&TMP2254 );
				if( TMP2264 == 0ll ) goto label288;
				{
					ylSTRING TMP2275;
					ylSTRING TMP2285;
					ylSTRING TMP2295;
					int64 TMP2385;
					int64 TMP2395;
					int64 TMP2405;
					ylSTRING TMP2425;
					ylSTRING TMP2435;
					ylSTRING TMP2455;
					ONUM = ONUM + 1ll;
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					label289:;
					int64 vr1090 = yl_StrLen( (void*)&TMP1, -1ll );
					if( I1 > vr1090 ) goto label290;
					{
						I1 = I1 + 1ll;
						ylSTRING* vr1093 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						int32 vr1094 = yl_StrCompare( (void*)vr1093, -1ll, (void*)"(", 2ll );
						if( (int64)vr1094 != 0ll ) goto label292;
						{
							goto label290;
							label292:;
						}
						ylSTRING* vr1097 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
						yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1097, -1ll, 0 );
					}
					goto label289;
					label290:;
					ylSTRING LOOPNAME5;
					__builtin_memset( &LOOPNAME5, 0, 24ll );
					__builtin_memset( &TMP2295, 0, 24ll );
					__builtin_memset( &TMP2285, 0, 24ll );
					yl_StrAssign( (void*)&TMP2285, -1ll, (void*)"", 1ll, 0 );
					__builtin_memset( &TMP2275, 0, 24ll );
					yl_StrAssign( (void*)&TMP2275, -1ll, (void*)" ", 2ll, 0 );
					ylSTRING* vr1108 = REPLACE( &TMP21, &TMP2275, &TMP2285 );
					yl_StrAssign( (void*)&TMP2295, -1ll, (void*)vr1108, -1ll, 0 );
					ylSTRING* vr1111 = AHR( &TMP2295 );
					yl_StrAssign( (void*)&LOOPNAME5, -1ll, (void*)vr1111, -1ll, 0 );
					yl_StrDelete( (ylSTRING*)&TMP2295 );
					yl_StrDelete( (ylSTRING*)&TMP2285 );
					yl_StrDelete( (ylSTRING*)&TMP2275 );
					yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
					ylSTRING* vr1119 = yl_StrMid( (ylSTRING*)&TMP1, I1 + 1ll, 1ll );
					int32 vr1120 = yl_StrCompare( (void*)vr1119, -1ll, (void*)")", 2ll );
					if( (int64)vr1120 == 0ll ) goto label294;
					{
						label295:;
						int64 vr1123 = yl_StrLen( (void*)&TMP1, -1ll );
						if( I1 > vr1123 ) goto label296;
						{
							yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
							label297:;
							int64 vr1126 = yl_StrLen( (void*)&TMP1, -1ll );
							if( I1 > vr1126 ) goto label298;
							{
								yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
								I1 = I1 + 1ll;
								ylSTRING* vr1130 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1131 = yl_StrCompare( (void*)vr1130, -1ll, (void*)")", 2ll );
								if( (int64)vr1131 != 0ll ) goto label300;
								{
									MODELTMP1 = 1ll;
									goto label298;
								}
								label300:;
								label299:;
								ylSTRING* vr1134 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1135 = yl_StrCompare( (void*)vr1134, -1ll, (void*)"+", 2ll );
								if( (int64)vr1135 != 0ll ) goto label302;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"+", 2ll, 0 );
								}
								label302:;
								label301:;
								ylSTRING* vr1139 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1140 = yl_StrCompare( (void*)vr1139, -1ll, (void*)"&", 2ll );
								if( (int64)vr1140 != 0ll ) goto label304;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"&", 2ll, 0 );
								}
								label304:;
								label303:;
								ylSTRING* vr1144 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1145 = yl_StrCompare( (void*)vr1144, -1ll, (void*)"-", 2ll );
								if( (int64)vr1145 != 0ll ) goto label306;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"-", 2ll, 0 );
								}
								label306:;
								label305:;
								ylSTRING* vr1149 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1150 = yl_StrCompare( (void*)vr1149, -1ll, (void*)"*", 2ll );
								if( (int64)vr1150 != 0ll ) goto label308;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"*", 2ll, 0 );
								}
								label308:;
								label307:;
								ylSTRING* vr1154 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1155 = yl_StrCompare( (void*)vr1154, -1ll, (void*)"/", 2ll );
								if( (int64)vr1155 != 0ll ) goto label310;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"/", 2ll, 0 );
								}
								label310:;
								label309:;
								ylSTRING* vr1159 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1160 = yl_StrCompare( (void*)vr1159, -1ll, (void*)"\x5C", 2ll );
								if( (int64)vr1160 != 0ll ) goto label312;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"\x5C", 2ll, 0 );
								}
								label312:;
								label311:;
								ylSTRING* vr1164 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1165 = yl_StrCompare( (void*)vr1164, -1ll, (void*)"|", 2ll );
								if( (int64)vr1165 != 0ll ) goto label314;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"|", 2ll, 0 );
								}
								label314:;
								label313:;
								ylSTRING* vr1169 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1170 = yl_StrCompare( (void*)vr1169, -1ll, (void*)"=", 2ll );
								if( (int64)vr1170 != 0ll ) goto label316;
								{
									yl_StrAssign( (void*)&MATHSIGN1, -1ll, (void*)"=", 2ll, 0 );
								}
								label316:;
								label315:;
								ylSTRING* vr1174 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1175 = yl_StrCompare( (void*)vr1174, -1ll, (void*)"<", 2ll );
								if( (int64)vr1175 != 0ll ) goto label318;
								{
									int64 TMP2309;
									int64 TMP2319;
									int64 TMP2329;
									int64 TMP2339;
									yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
									label319:;
									int64 vr1179 = yl_StrLen( (void*)&TMP1, -1ll );
									if( I1 > vr1179 ) goto label320;
									{
										I1 = I1 + 1ll;
										ylSTRING* vr1182 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
										int32 vr1183 = yl_StrCompare( (void*)vr1182, -1ll, (void*)">", 2ll );
										if( (int64)vr1183 != 0ll ) goto label322;
										{
											goto label320;
											label322:;
										}
										ylSTRING* vr1186 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
										yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1186, -1ll, 0 );
									}
									goto label319;
									label320:;
									TMP2309 = F21;
									ylSTRING* vr1188 = yl_StrAllocTempDescZEx( (uint8*)"add", 3ll );
									yl_PrintString( (int32)TMP2309, (ylSTRING*)vr1188, 1 );
									TMP2319 = F21;
									ylSTRING* vr1191 = AHR( &TMP21 );
									yl_PrintString( (int32)TMP2319, (ylSTRING*)vr1191, 1 );
									TMP2329 = F21;
									ylSTRING* vr1193 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
									yl_PrintString( (int32)TMP2329, (ylSTRING*)vr1193, 1 );
									TMP2339 = F21;
									yl_PrintString( (int32)TMP2339, (ylSTRING*)&MATHSIGN1, 1 );
									yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
								}
								label318:;
								label317:;
								ylSTRING* vr1199 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
								int32 vr1200 = yl_StrCompare( (void*)vr1199, -1ll, (void*)"\x22", 2ll );
								if( (int64)vr1200 != 0ll ) goto label324;
								{
									int64 TMP2349;
									int64 TMP2359;
									int64 TMP2369;
									int64 TMP2379;
									yl_StrAssign( (void*)&MATHTMP1, -1ll, (void*)"", 1ll, 0 );
									label325:;
									int64 vr1204 = yl_StrLen( (void*)&TMP1, -1ll );
									if( I1 > vr1204 ) goto label326;
									{
										I1 = I1 + 1ll;
										ylSTRING* vr1207 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
										int32 vr1208 = yl_StrCompare( (void*)vr1207, -1ll, (void*)"\x22", 2ll );
										if( (int64)vr1208 != 0ll ) goto label328;
										{
											goto label326;
											label328:;
										}
										ylSTRING* vr1211 = yl_StrMid( (ylSTRING*)&TMP1, I1, 1ll );
										yl_StrConcatAssign( (void*)&TMP21, -1ll, (void*)vr1211, -1ll, 0 );
									}
									goto label325;
									label326:;
									TMP2349 = F21;
									ylSTRING* vr1213 = yl_StrAllocTempDescZEx( (uint8*)"push", 4ll );
									yl_PrintString( (int32)TMP2349, (ylSTRING*)vr1213, 1 );
									TMP2359 = F21;
									yl_PrintString( (int32)TMP2359, (ylSTRING*)&TMP21, 1 );
									TMP2369 = F21;
									ylSTRING* vr1217 = yl_StrAllocTempDescZEx( (uint8*)"api", 3ll );
									yl_PrintString( (int32)TMP2369, (ylSTRING*)vr1217, 1 );
									TMP2379 = F21;
									yl_PrintString( (int32)TMP2379, (ylSTRING*)&MATHSIGN1, 1 );
									yl_StrAssign( (void*)&TMP21, -1ll, (void*)"", 1ll, 0 );
								}
								label324:;
								label323:;
							}
							goto label297;
							label298:;
							if( MODELTMP1 != 1ll ) goto label330;
							{
								MODELTMP1 = 0ll;
								goto label296;
							}
							label330:;
							label329:;
						}
						goto label295;
						label296:;
					}
					label294:;
					label293:;
					TMP2385 = F21;
					ylSTRING* vr1222 = yl_StrAllocTempDescZEx( (uint8*)"loop", 4ll );
					yl_PrintString( (int32)TMP2385, (ylSTRING*)vr1222, 1 );
					TMP2395 = F21;
					yl_PrintString( (int32)TMP2395, (ylSTRING*)&LOOPNAME5, 1 );
					TMP2405 = F21;
					__builtin_memset( &TMP2425, 0, 24ll );
					ylSTRING* vr1229 = yl_StrConcat( &TMP2425, (void*)"<loop.", 7ll, (void*)&LOOPNAME5, -1ll );
					__builtin_memset( &TMP2435, 0, 24ll );
					ylSTRING* vr1232 = yl_StrConcat( &TMP2435, (void*)vr1229, -1ll, (void*)">", 2ll );
					yl_PrintString( (int32)TMP2405, (ylSTRING*)vr1232, 1 );
					yl_StrAssign( (void*)&Y21, -1ll, (void*)"loop.", 6ll, 0 );
					yl_StrConcatAssign( (void*)&Y21, -1ll, (void*)&LOOPNAME5, -1ll, 0 );
					__builtin_memset( &TMP2455, 0, 24ll );
					yl_StrAssign( (void*)&TMP2455, -1ll, (void*)"", 1ll, 0 );
					MAKEFILE( F11, F21, &Y21, &TMP2455 );
					yl_StrDelete( (ylSTRING*)&TMP2455 );
					yl_StrDelete( (ylSTRING*)&LOOPNAME5 );
				}
				label288:;
				label287:;
			}
			label202:;
			label201:;
			int64 vr1244 = yl_StrLen( (void*)&TMP1, -1ll );
			if( I1 <= vr1244 ) goto label332;
			{
				ERRCATCH( LINEN );
			}
			label332:;
			label331:;
			int64 vr1246 = yl_StrLen( (void*)&TMP1, -1ll );
			if( I1 != vr1246 ) goto label334;
			{
				goto label60;
				label334:;
			}
		}
		goto label59;
		label60:;
	}
	goto label57;
	label58:;
	yl_StrDelete( (ylSTRING*)&OBCASE1 );
	yl_StrDelete( (ylSTRING*)&OBJNAME1 );
	yl_StrDelete( (ylSTRING*)&OBNAME1 );
	yl_StrDelete( (ylSTRING*)&LIBNAME1 );
	yl_StrDelete( (ylSTRING*)&CLASSSNAME1 );
	yl_StrDelete( (ylSTRING*)&CLASSFNAME1 );
	yl_StrDelete( (ylSTRING*)&MATHTMP1 );
	yl_StrDelete( (ylSTRING*)&MATHNUMSIGN1 );
	yl_StrDelete( (ylSTRING*)&MATHSIGN1 );
	yl_StrDelete( (ylSTRING*)&Y21 );
	yl_StrDelete( (ylSTRING*)&YURONC1 );
	yl_StrDelete( (ylSTRING*)&VALUE1 );
	yl_StrDelete( (ylSTRING*)&COUNTSIGN1 );
	yl_StrDelete( (ylSTRING*)&COMNAME1 );
	yl_StrDelete( (ylSTRING*)&SIGN1 );
	yl_StrDelete( (ylSTRING*)&TMP21 );
	yl_StrDelete( (ylSTRING*)&TMP1 );
	label56:;
}

__attribute__(( constructor )) static void yl_ctor__yuronc( void )
{
	label0:;
	MAIN(  );
	yl_End( 0 );
	label1:;
}
