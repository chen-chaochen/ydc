# Yuron语言组件/Yuron汉语编程

#### 介绍

  Yuron 是一款简单易学,轻量级,跨平台,拓展性强,支持[汉语编程](https://gitee.com/chen-chaochen/ydc/tree/master/%E6%B1%89%E8%AF%AD%E7%A4%BA%E4%BE%8B)(GBK)及DIY,面向对象的高级编程语言。

  Yuron拥有简洁的语法,轻量级的开发环境,便利的拓展性,主流操作系统的兼容性,面向对象的编程方式,

较高的DIY自由度,优秀的跨平台能力。适用于编程入门,学习操作系统及编译原理,[嵌入式开发](https://gitee.com/chen-chaochen/ydc/tree/master/%E5%B5%8C%E5%85%A5%E5%BC%8F%E5%BC%80%E5%8F%91)嵌入式开发等多种场景。


#### yuron_dev 适合开发者的版本
适用于玉龙的二次开发与移植
详见 [仓库 yuron_dev](https://gitee.com/chen-chaochen/yuron_dev)

  ![用Yuron编写小游戏](example/%E7%A1%AC%E5%B8%81%E6%B8%B8%E6%88%8F.png)
![输入图片说明](%E6%B1%89%E8%AF%AD%E7%A4%BA%E4%BE%8B/%E7%A4%BA%E4%BE%8B/%E4%BA%95%E5%AD%97%E6%A3%8B%E6%B8%B8%E6%88%8F/%E4%BA%95%E5%AD%97%E6%A3%8B%E6%B8%B8%E6%88%8F.png)



#### 软件架构

##### YDC('Yu-ron dev-collections)

   yuronc  编译器

   yuron   解释器

   libroot  yu-ron LIB文件库

   tmp 编译时临时文件库


#### 安装教程

系统要求:


  .linux:

  不被作为init进程即可(会引发"内核恐慌")

  如需支持汉语,须安装GBK编码

  .Windows:

  安装有Microsoft C routine

安装方法
  1.下载对应系统的发行版本压缩包

  2.解压到需要的文件夹

  3-1(Windows用户) 点击Install.bat根据向导编译安装

  3-2-1(Linux用户) 复制压缩包内文件到/usr/local/bin 

  3-2-2 编译位于source目录内的基本类

  3-2-3 复制基本类至libroot中

//以下为LYRIC及更早版本的方法
  1.下载对应系统的发行版本压缩包

  2.解压到需要的文件夹

  3.复制压缩包内文件到/usr/local/bin
    
    效果
       
       文件 usr/local/bin/yuron usr/local/bin/yuron

       目录 usr/local/bin/tmp usr/local/bin/conf usr/local/bin/libroot

     Windows用户自行创建环境变量 yuron = yuron.exe yuronc = yuronc.exe


### Yuron的特点

#### 独立设计,告别加壳
   自主的 源代码 => 字符/字节码 架构,编译器直接输出Yuron专用的字节码语言。<br>Yuron的编译算法,解释算法,语法结构,YIL_Framework,均为独立设计。
#### 灵活多变,个性十足
   Yuron采用模块化,配置化,标识化的编译算法与数据封装机制.<br>用户可以自定义(包括但不仅限于):<br>1.语言关键词;<br>2.语言语法习惯;<br>3.语言语法风格;<br>比如<br>你可以选择类似C++的语法:<br>
   `#棋盘.摆棋();`<br>
你也可以选择更符合汉语的语法:<br>`#将棋盘摆好棋();`<br>甚至文言语法<br>`#屏幕之输入();`<br>例见 汉语示例\示例\井字棋游戏<br><br>其次yuron支持汉语编程,而不仅仅局限于汉语编程,通过修改位于 .\conf 目录下的配置文件,yuron可以变成你所喜欢的任何语言
#### 良好的跨平台设计、通用性与拓展性
   Yuron采用 编译/解释 分离设计,无论是何种工作平台,只要有为其编写的Yuron解释器,都可以正常运行Yuron程序.<br>其次Yuronc的编译算法也是独特的:<br>在编译时,Yuronc会将所需类库中的被调用的函数单独分离后再添加入输出文件中.<br>这一算法即避免了yuron程序对支持库的运行依赖,也避免了全库链接引起文件过大<br>同时,即使您使用不同的配置文件编译器输出的类库也是可以兼容的。<br>满足不同人员编程的通用性<br>拓展yuron功能时,仅需拓展解释器中api标签所支持的功能,同时用yuron编写接口类即可。
![玉龙小程序](%E6%B1%89%E8%AF%AD%E7%A4%BA%E4%BE%8B/%E7%A4%BA%E4%BE%8B/%E7%AF%AE%E7%90%83/%E7%AF%AE%E7%90%83%E6%AF%94%E8%B5%9B.png)
#### 开源、免费、自由
   Yuron是免费的开源软件,在未来一段时间内都会保持当前的开源状态.Yuron致力于提供高自由、多功能、跨平台、低门槛的编程解决方案;<br>
![编译](support/sources.png)
### 软件说明




  Yuron(玉龙汉语编程) 是一款简单易学,跨平台,拓展性强,支持汉语(GBK),面向对象的高级编程语言。

  Yuron可以编写运行于yuron平台的Yuron应用程序(Yuron Application/0.3版本以后为.yl_app格式),

其通常运行于命令行界面。

  与一些常见语言一样,Yuron compiler是将源代码编译为与硬件无关的中间语言

(IL),然后由为不同平台编写的解释器在不同平台上解释执行。

当开始编译Yuron源代码时,Yuronc会将其编译成与硬件结构无关的中间语言。与Java,python

等不同,yuron的中间代码为ascii格式文本代码,可以用文本编辑器修改。yuron的开

发需要yuron开发组件(yuron dev-collection),当前,YDC拥有5个原始发行环境:

  .WINDOWS-x86  

  .WINDOWS-x86_64

  .LINUX-X86

  .LINUX-X86_64

  .LINUX for Raspi


  一个yuron平台包括实际计算机,yuron解释器，API。yuron程序可在任何一个合规的yuron平台

上通过解释器调用API解释执行。

  yuron的关键词全部以.conf文件的形式储存于./conf文件夹中,有能力的用户可以自由更改

(如修改为汉语,应使用ascii格式,GBK编码)

发布时关键词全为英语(0.3版本以后包含汉语)。用户可以DIY自己的语言,编译时不同用户即使使用不同关键词,

其编译后的库文件也可以兼容。本示例中程序皆指默认关键词。

  现阶段,Yuron专用的IDE尚未开发完成,!您可以使用VS Code等IDE编写Yuron代码。

  Yuron 的解释器可有任意语言拓展或重写,有能力的用户可以方便的对自己所需功能进行专项

拓展以更好地满足个人需求.

 **注意** :yuron的关键词,变量名,类名,方法名皆区分大小写

#### Yuron及YIL

Yuron:

  yuron 是YDC的核心,作为解释器,它会解释文件中的玉龙虚拟指令(YVI)(0.4以下版本为玉龙中间语言:YIL)

以运行Yuron程序。源码中开放了Yuron的GCC实现方法，用户可以按照自己的需求

对解释器进行更改

使用方法为#bash:yuron [文件全名] 示例(假设为/tmp/Hello,

且拥有/tmp/Hello.conf)

#bash-$:cd /tmp

#bash-/tmp$:yuron Hello

yuron会读取[文件].conf中的入口点(load point)并以此开始解释指定文件

YVI/YIL：


  YVI/YIL是编译器输出的类汇编指令语言，可在源代码中由[ /]插入(由于编译器会转换

变量名,插入变量时应用 $[ /]).

  基本指令如下

- <> </ >    标签、结束标签

- <loop.>    循环标签

- <logic.>  逻辑标签

- [exit/]  退出循环

- [push/] [] 将常量压入主栈

- [add/] []  将一个变量压入主栈

- [mov/] []  将栈顶元素移入指定储存空间

- [pop/] 弹栈

- [loop/] [] 从栈顶获取循环次数(-1 = 无穷),执行指定循环

- [logic/] [] 从栈顶获取逻辑参数，执行指定逻辑语句

- [goto] [] 跳转至标签 label$:[]

- [api/] [] 按压入顺序先-后获取栈中元素,依次作为参数执行指定API函数,并将结果压栈

- [thread/] 仅限YVI 启动线程

 API函数一览表见文件 [链接](https://gitee.com/chen-chaochen/ydc/blob/master/%E6%94%AF%E6%8C%81/%E7%8E%89%E9%BE%99%E8%99%9A%E6%8B%9F%E6%8C%87%E4%BB%A4%E9%9B%86.txt)




#### yuronc

  yuronc是YDC的主要组件,它可以将可读性高的yuron源码文件(.yuron)编译成YIL以供解释器执行。

yuronc编译时会将源码中引用的支持库文件与输出文件静态整合。因此,yuronc输出的APP

或LIB在移植时不需移植编译时支持库。

  yuronc使用示例如下(假设文件为/tmp/Hello.yuron)

#bash-$:cd /tmp

#bash-/tmp$:yuronc Hello     不加拓展名

输出文件为 /tmp/Hello (如给定了load point,则还有/tmp/Hello.conf)

#### 可执行文件的生成

在0.3.6版本中YDC添加的yuron_exe组件,可以根据编译后的yuron应用生成本地可执行程序<br>
![输入图片说明](%E6%B1%89%E8%AF%AD%E7%A4%BA%E4%BE%8B/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202022-06-30%20164532.png)


#### yuron语法

##### 基本规则

1.一行中包含多个语句,语句间必须用;隔开

2.除声明语句外,多个语句可以写在一行

3.编码应为GBK-ASCII格式

4.标识符默认为半角标识符

##### 数据结构

1.常量与变量

  常量用""扩起 示例"Hello"

  变量用<>扩起 示例<var1>

  yuron的变量默认皆为全局变量,最多可支持10000000个变量。


2.类与方法



 类的声明  +class 类名,父类名{
 示例

 +class class1,Object{

   //内部声明

 }

 yuron的类皆为Object类的子类,用户自定义类也应继承自Object;

 类在使用前应先实例化为对象,对象与类可以重名 示例 !class1 = class1;

 类内方法：

  类内只有一种方法 void,其自身无返回值,但可通过同名变量返回值。

  方法声明 +void 方法名(参数){

  示例

  +class class1,Object{

   +void main(){

   //语句  

   }

  }






  数据封装方法 [this]

  示例 <[this].var1>


  则var1在被其他类调用时应写作<对象名.var1>


  由于Yuron区分大小写,为了程序可读性,我们约定：

   1.以大写字母开头命名类 例 Class1；

   2.以小写字母开头命名方法与变量 例 Class1.void1()

   3.以全大写字母命名语句

   4.继承标志默认为. 您也可以直接用任意字符表示继承


#### 控制语句
1.@load-point; 用以指定程序入口点

2.+include filepath； 用以载入libroot中的指定文件
  +import file path; 载入同目录下源文件(不用带拓展名,默认为.yuron文件)

3.+if  条件语句

  用法

  +if name([变量/常量]  [条件 >,>=,<,<=,=(字符),==(数值),!=(字符),!==(数值)] [变量/常量] ){

   //语句内容

  }

 如果条件成立,则执行语句内容

4.+loop 循环语句

 用法 

+loop name([常量/变量]){

 //语句内容

} 

 执行指定次数内容(小于0视为无穷次)

5.#  调用对象中的方法 示例 #Console.print("hello");

6.赋值语句 <var1> = <var2>/"con1" [+,-,*,/,\,|,&] ……

  注意运算优先级为从左到右,并非*优于+

7.注释 // 后一行皆为注释;/* 与 */一行之间皆为注释

8.语句命名规则：

   不同类型语句可以重名,相同类型语句不应重名,语句也可由[this]进行封装

   为了程序可读性,我们约定: 
     
   1.除非一个类作为程序使用,否者其类内语句应用[this]进行封装；

   2.除非一个类作为程序使用,否则其类内条件语句应在封装后以LOGIC.开头;  示例  [this].LOGIC.S1()

   3.除非一个类作为程序使用,否则其类内循环语句应在封装后以LOOP.开头;  示例  [this].LOOP.S1()

   4.以全大写字母命名语句;

9.基本类

  基本类是YDC发布时自带的一些类,定义了一些基本的IO与功能,用以初学者基本的开发

  有关基本类的命名与方法, 详见 /support/classlist.txt文件


### 第一个程序 hello,world！

![你好世界](example/%E4%BD%A0%E5%A5%BD%E4%B8%96%E7%95%8C.png)

用文本编辑器编辑如下代码


```
//这是注释
//文件名 Helloworld.yuron

//定义load-point 为 main()
@main;

//引用基本类
+include yuron/Console;

//编写用户类
+class Hello{
    +void main(){
        !Console = Console;
        #Console.print("Hello,World!\n")
    }
}
```



打开终端

#bash-$:cd /tmp

#bash-/tmp$:yuronc Helloworld

#bash-/tmp$:yuron Helloworld

Hello,world!

#bash-/tmp$:
<br><br>汉语版示例:<br>

```
@开始;
+引用 玉龙\控制台;
+方法 开始(){
    !控制台 = 控制台;
    #控制台.输出("你好,Yuron编程世界!");
}
```




### Yuron的移植与再发布

Yuron是免费的开源软件，致力于打造自由的DIY编程语言。<br>
YDC组件源代码遵循GPL3.0开源协议。＜br＞
Yuron_framework、YIL、YVI皆遵循玉龙使用协议1.0


### 加入Yuron
  Yuron对开发者持以热情的欢迎态度,任何合法组织与个人都被欢迎参与对Yuron的改进;



如有其他需求，请联系开发者














 
 
 
 
 
 